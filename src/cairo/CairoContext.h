
PHP_METHOD(CairoContext, __construct);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext____construct_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
	ZEND_ARG_OBJ_INFO(0, obj, CairoSurface, 1)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, appendPath);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__append_path_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_OBJ_INFO(0, p, CairoPath, 1)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, arc);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__arc_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 5)
  ZEND_ARG_INFO(0, xc)
  ZEND_ARG_INFO(0, yc)
  ZEND_ARG_INFO(0, radius)
  ZEND_ARG_INFO(0, angle1)
  ZEND_ARG_INFO(0, angle2)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, arcNegative);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__arc_negative_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 5)
  ZEND_ARG_INFO(0, xc)
  ZEND_ARG_INFO(0, yc)
  ZEND_ARG_INFO(0, radius)
  ZEND_ARG_INFO(0, angle1)
  ZEND_ARG_INFO(0, angle2)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, clip);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__clip_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, clipExtents);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__clip_extents_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, clipPreserve);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__clip_preserve_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, closePath);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__close_path_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, copyClipRectangleList);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__copy_clip_rectangle_list_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, copyPage);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__copy_page_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, copyPath);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__copy_path_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, copyPathFlat);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__copy_path_flat_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, curveTo);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__curve_to_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 6)
  ZEND_ARG_INFO(0, x1)
  ZEND_ARG_INFO(0, y1)
  ZEND_ARG_INFO(0, x2)
  ZEND_ARG_INFO(0, y2)
  ZEND_ARG_INFO(0, x3)
  ZEND_ARG_INFO(0, y3)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, deviceToUser);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__device_to_user_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, x)
  ZEND_ARG_INFO(0, y)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, deviceToUserDistance);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__device_to_user_distance_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, x)
  ZEND_ARG_INFO(0, y)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, fill);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__fill_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, fillExtents);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__fill_extents_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, fillPreserve);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__fill_preserve_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, fontExtents);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__font_extents_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, getAntialias);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_antialias_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, getCurrentPoint);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_current_point_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, getDash);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_dash_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, getDashCount);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_dash_count_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, getFillRule);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_fill_rule_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, getFontFace);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_font_face_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, getFontMatrix);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_font_matrix_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, getFontOptions);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_font_options_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, getGroupTarget);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_group_target_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, getLineCap);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_line_cap_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, getLineJoin);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_line_join_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, getLineWidth);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_line_width_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, getMatrix);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_matrix_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, getMiterLimit);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_miter_limit_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, getOperator);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_operator_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, getScaledFont);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_scaled_font_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, getSource);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_source_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, getTarget);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_target_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, getTolerance);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_tolerance_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, glyphExtents);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__glyph_extents_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
#if (PHP_MINOR_VERSION > 0)  
  ZEND_ARG_ARRAY_INFO(0, obj, 1)
#else
  ZEND_ARG_INFO(0, obj)
#endif
  ZEND_ARG_INFO(0, num)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, glyphPath);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__glyph_path_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
#if (PHP_MINOR_VERSION > 0)
  ZEND_ARG_ARRAY_INFO(0, obh, 1)
#else
  ZEND_ARG_INFO(0, obh);
#endif
  ZEND_ARG_INFO(0, num)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, hasCurrentPoint);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__has_current_point_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, identityMatrix);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__identity_matrix_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, inFill);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__in_fill_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, x)
  ZEND_ARG_INFO(0, y)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, inStroke);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__in_stroke_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, x)
  ZEND_ARG_INFO(0, y)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, lineTo);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__line_to_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, x)
  ZEND_ARG_INFO(0, y)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, mask);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__mask_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_OBJ_INFO(0, p, CairoPattern, 1)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, maskSurface);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__mask_surface_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_OBJ_INFO(0, s, CairoSurface, 1)
  ZEND_ARG_INFO(0, surface_x)
  ZEND_ARG_INFO(0, surface_y)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, moveTo);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__move_to_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, x)
  ZEND_ARG_INFO(0, y)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, newPath);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__new_path_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, newSubPath);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__new_sub_path_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, paint);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__paint_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, paintWithAlpha);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__paint_with_alpha_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, alpha)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, pathExtents);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__path_extents_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
  ZEND_ARG_OBJ_INFO(0, path, CairoPath, 1)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, popGroup);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__pop_group_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, popGroupToSource);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__pop_group_to_source_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, pushGroup);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__push_group_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, pushGroupWithContent);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__push_group_with_content_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, content)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, rectangle);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__rectangle_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 4)
  ZEND_ARG_INFO(0, x)
  ZEND_ARG_INFO(0, y)
  ZEND_ARG_INFO(0, width)
  ZEND_ARG_INFO(0, height)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, relCurveTo);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__rel_curve_to_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 6)
  ZEND_ARG_INFO(0, x1)
  ZEND_ARG_INFO(0, y1)
  ZEND_ARG_INFO(0, x2)
  ZEND_ARG_INFO(0, y2)
  ZEND_ARG_INFO(0, x3)
  ZEND_ARG_INFO(0, y3)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, relLineTo);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__rel_line_to_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, x)
  ZEND_ARG_INFO(0, y)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, relMoveTo);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__rel_move_to_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, x)
  ZEND_ARG_INFO(0, y)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, resetClip);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__reset_clip_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, restore);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__restore_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, rotate);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__rotate_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, angle)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, save);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__save_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, scale);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__scale_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, x)
  ZEND_ARG_INFO(0, y)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, selectFontFace);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__select_font_face_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, family)
  ZEND_ARG_INFO(0, slant)
  ZEND_ARG_INFO(0, weight)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, setAntialias);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_antialias_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
  ZEND_ARG_INFO(0, antialias)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, setDash);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_dash_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
#if (PHP_MINOR_VERSION > 0)
  ZEND_ARG_ARRAY_INFO(0, dashes, 1)
#else
  ZEND_ARG_INFO(0, dashes)
#endif
  ZEND_ARG_INFO(0, num_dashes)
  ZEND_ARG_INFO(0, offset)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, setFillRule);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_fill_rule_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, fill_rule)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, setFontFace);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_font_face_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
  ZEND_ARG_OBJ_INFO(0, obj, CairoFontFace, 1)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, setFontMatrix);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_font_matrix_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_OBJ_INFO(0, matrix, CairoMatrix, 1)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, setFontOptions);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_font_options_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_OBJ_INFO(0, options, CairoFontOptions, 1)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, setFontSize);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_font_size_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, size)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, setLineCap);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_line_cap_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, line_cap)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, setLineJoin);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_line_join_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, line_join)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, setLineWidth);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_line_width_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, width)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, setMatrix);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_matrix_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_OBJ_INFO(0, matix, CairoMatrix, 1)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, setMiterLimit);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_miter_limit_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, limit)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, setOperator);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_operator_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, op)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, setSource);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_source_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
  ZEND_ARG_OBJ_INFO(0, p, CairoPattern, 1)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, setSourceRgb);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_source_rgb_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 3)
  ZEND_ARG_INFO(0, red)
  ZEND_ARG_INFO(0, green)
  ZEND_ARG_INFO(0, blue)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, setSourceRgba);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_source_rgba_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 3)
  ZEND_ARG_INFO(0, red)
  ZEND_ARG_INFO(0, green)
  ZEND_ARG_INFO(0, blue)
  ZEND_ARG_INFO(0, alpha)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, setSourceSurface);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_source_surface_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_OBJ_INFO(0, surface, CairoSurface, 1)
  ZEND_ARG_INFO(0, x)
  ZEND_ARG_INFO(0, y)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, setTolerance);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_tolerance_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, tolerance)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, showGlyphs);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__show_glyphs_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
#if (PHP_MINOR_VERSION > 0)
  ZEND_ARG_ARRAY_INFO(0, obj, 1)
#else
  ZEND_ARG_INFO(0, obj)
#endif
  ZEND_ARG_INFO(0, num_glyphs)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, showPage);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__show_page_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, showText);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__show_text_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, obj)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, stroke);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__stroke_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, strokeExtents);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__stroke_extents_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, strokePreserve);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__stroke_preserve_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, textExtents);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__text_extents_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, str)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, textPath);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__text_path_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, obj)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, transform);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__transform_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_OBJ_INFO(0, matrix, CairoMatrix, 1)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, translate);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__translate_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, tx)
  ZEND_ARG_INFO(0, ty)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, userToDevice);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__user_to_device_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, x)
  ZEND_ARG_INFO(0, y)
ZEND_END_ARG_INFO()
#endif

PHP_METHOD(CairoContext, userToDeviceDistance);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__user_to_device_distance_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, dx)
  ZEND_ARG_INFO(0, dy)
ZEND_END_ARG_INFO()
#endif
