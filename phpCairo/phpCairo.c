/*
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.0 of the PHP license,       |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_0.txt.                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
   | Authors: Akshat Gupta <g.akshat@gmail.com>                           |
   +----------------------------------------------------------------------+
*/

/* $ Id: $ */ 

#include "php_phpCairo.h"

#if HAVE_PHPCAIRO

/* {{{ Class definitions */

/* {{{ Class CairoContext */

static zend_class_entry * CairoContext_ce_ptr = NULL;

/* {{{ Methods */


/* {{{ proto void append_path(object p)
   */
PHP_METHOD(CairoContext, append_path)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	zval * p = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Oo", &_this_zval, CairoContext_ce_ptr, &p) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "append_path: not yet implemented"); RETURN_FALSE;

}
/* }}} append_path */



/* {{{ proto void arc(float xc, float yc, float radius, float angle1, float angle2)
   */
PHP_METHOD(CairoContext, arc)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double xc = 0.0;
	double yc = 0.0;
	double radius = 0.0;
	double angle1 = 0.0;
	double angle2 = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Oddddd", &_this_zval, CairoContext_ce_ptr, &xc, &yc, &radius, &angle1, &angle2) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "arc: not yet implemented"); RETURN_FALSE;

}
/* }}} arc */



/* {{{ proto void arc_negative(float xc, float yc, float radius, float angle1, float angle2)
   */
PHP_METHOD(CairoContext, arc_negative)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double xc = 0.0;
	double yc = 0.0;
	double radius = 0.0;
	double angle1 = 0.0;
	double angle2 = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Oddddd", &_this_zval, CairoContext_ce_ptr, &xc, &yc, &radius, &angle1, &angle2) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "arc_negative: not yet implemented"); RETURN_FALSE;

}
/* }}} arc_negative */



/* {{{ proto void clip()
   */
PHP_METHOD(CairoContext, clip)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "clip: not yet implemented"); RETURN_FALSE;

}
/* }}} clip */



/* {{{ proto array clip_extents()
   */
PHP_METHOD(CairoContext, clip_extents)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	array_init(return_value);

	php_error(E_WARNING, "clip_extents: not yet implemented"); RETURN_FALSE;

	array_init(return_value);
}
/* }}} clip_extents */



/* {{{ proto void clip_preserve()
   */
PHP_METHOD(CairoContext, clip_preserve)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "clip_preserve: not yet implemented"); RETURN_FALSE;

}
/* }}} clip_preserve */



/* {{{ proto void close_path()
   */
PHP_METHOD(CairoContext, close_path)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "close_path: not yet implemented"); RETURN_FALSE;

}
/* }}} close_path */



/* {{{ proto object copy_clip_rectangle_list()
   */
PHP_METHOD(CairoContext, copy_clip_rectangle_list)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	do {
		/*using iterator in php5.0 to store the 
						list. Pass the array to an iterator during the 
						object definition and jobs done */
	} while (0);
}
/* }}} copy_clip_rectangle_list */



/* {{{ proto void copy_page()
   */
PHP_METHOD(CairoContext, copy_page)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "copy_page: not yet implemented"); RETURN_FALSE;

}
/* }}} copy_page */



/* {{{ proto object copy_path()
   */
PHP_METHOD(CairoContext, copy_path)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "copy_path: not yet implemented"); RETURN_FALSE;

	object_init(return_value)
}
/* }}} copy_path */



/* {{{ proto object copy_path_flat()
   */
PHP_METHOD(CairoContext, copy_path_flat)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "copy_path_flat: not yet implemented"); RETURN_FALSE;

	object_init(return_value)
}
/* }}} copy_path_flat */



/* {{{ proto void curve_to (float x1, float y1, float x2, float y2, float x3, float y3)
   */
PHP_METHOD(CairoContext, curve_to)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double x1 = 0.0;
	double y1 = 0.0;
	double x2 = 0.0;
	double y2 = 0.0;
	double x3 = 0.0;
	double y3 = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Odddddd", &_this_zval, CairoContext_ce_ptr, &x1, &y1, &x2, &y2, &x3, &y3) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "curve_to: not yet implemented"); RETURN_FALSE;

}
/* }}} curve_to */



/* {{{ proto array device_to_user(float x, float y)
   */
PHP_METHOD(CairoContext, device_to_user)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double x = 0.0;
	double y = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Odd", &_this_zval, CairoContext_ce_ptr, &x, &y) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	array_init(return_value);

	php_error(E_WARNING, "device_to_user: not yet implemented"); RETURN_FALSE;

	array_init(return_value);
}
/* }}} device_to_user */



/* {{{ proto array device_to_user_distance(float x, float y)
   */
PHP_METHOD(CairoContext, device_to_user_distance)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double x = 0.0;
	double y = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Odd", &_this_zval, CairoContext_ce_ptr, &x, &y) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	array_init(return_value);

	php_error(E_WARNING, "device_to_user_distance: not yet implemented"); RETURN_FALSE;

	array_init(return_value);
}
/* }}} device_to_user_distance */



/* {{{ proto void fill()
   */
PHP_METHOD(CairoContext, fill)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "fill: not yet implemented"); RETURN_FALSE;

}
/* }}} fill */



/* {{{ proto array fill_extents()
   */
PHP_METHOD(CairoContext, fill_extents)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	array_init(return_value);

	php_error(E_WARNING, "fill_extents: not yet implemented"); RETURN_FALSE;

	array_init(return_value);
}
/* }}} fill_extents */



/* {{{ proto void fill_preserve()
   */
PHP_METHOD(CairoContext, fill_preserve)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "fill_preserve: not yet implemented"); RETURN_FALSE;

}
/* }}} fill_preserve */



/* {{{ proto array font_extents()
   */
PHP_METHOD(CairoContext, font_extents)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	array_init(return_value);

	php_error(E_WARNING, "font_extents: not yet implemented"); RETURN_FALSE;

	array_init(return_value);
}
/* }}} font_extents */



/* {{{ proto int get_antialias()
   */
PHP_METHOD(CairoContext, get_antialias)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_antialias: not yet implemented"); RETURN_FALSE;

	RETURN_LONG(0);
}
/* }}} get_antialias */



/* {{{ proto array get_current_point()
   */
PHP_METHOD(CairoContext, get_current_point)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	array_init(return_value);

	php_error(E_WARNING, "get_current_point: not yet implemented"); RETURN_FALSE;

	array_init(return_value);
}
/* }}} get_current_point */



/* {{{ proto array get_dash()
   */
PHP_METHOD(CairoContext, get_dash)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	array_init(return_value);

	php_error(E_WARNING, "get_dash: not yet implemented"); RETURN_FALSE;

	array_init(return_value);
}
/* }}} get_dash */



/* {{{ proto int get_dash_count()
   */
PHP_METHOD(CairoContext, get_dash_count)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_dash_count: not yet implemented"); RETURN_FALSE;

	RETURN_LONG(0);
}
/* }}} get_dash_count */



/* {{{ proto int get_fill_rule()
   */
PHP_METHOD(CairoContext, get_fill_rule)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_fill_rule: not yet implemented"); RETURN_FALSE;

	RETURN_LONG(0);
}
/* }}} get_fill_rule */



/* {{{ proto object get_font_face()
   */
PHP_METHOD(CairoContext, get_font_face)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_font_face: not yet implemented"); RETURN_FALSE;

	object_init(return_value)
}
/* }}} get_font_face */



/* {{{ proto object get_font_matrix()
   */
PHP_METHOD(CairoContext, get_font_matrix)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_font_matrix: not yet implemented"); RETURN_FALSE;

	object_init(return_value)
}
/* }}} get_font_matrix */



/* {{{ proto object get_font_options()
   */
PHP_METHOD(CairoContext, get_font_options)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_font_options: not yet implemented"); RETURN_FALSE;

	object_init(return_value)
}
/* }}} get_font_options */



/* {{{ proto object get_group_target()
   */
PHP_METHOD(CairoContext, get_group_target)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_group_target: not yet implemented"); RETURN_FALSE;

	object_init(return_value)
}
/* }}} get_group_target */



/* {{{ proto int get_line_cap()
   */
PHP_METHOD(CairoContext, get_line_cap)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_line_cap: not yet implemented"); RETURN_FALSE;

	RETURN_LONG(0);
}
/* }}} get_line_cap */



/* {{{ proto int get_line_join()
   */
PHP_METHOD(CairoContext, get_line_join)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_line_join: not yet implemented"); RETURN_FALSE;

	RETURN_LONG(0);
}
/* }}} get_line_join */



/* {{{ proto float get_line_width()
   */
PHP_METHOD(CairoContext, get_line_width)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_line_width: not yet implemented"); RETURN_FALSE;

	RETURN_DOUBLE(0.0);
}
/* }}} get_line_width */



/* {{{ proto object get_matrix()
   */
PHP_METHOD(CairoContext, get_matrix)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_matrix: not yet implemented"); RETURN_FALSE;

	object_init(return_value)
}
/* }}} get_matrix */



/* {{{ proto float get_matrix_limit()
   */
PHP_METHOD(CairoContext, get_matrix_limit)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_matrix_limit: not yet implemented"); RETURN_FALSE;

	RETURN_DOUBLE(0.0);
}
/* }}} get_matrix_limit */



/* {{{ proto int get_operator()
   */
PHP_METHOD(CairoContext, get_operator)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_operator: not yet implemented"); RETURN_FALSE;

	RETURN_LONG(0);
}
/* }}} get_operator */



/* {{{ proto object get_scaled_font()
   */
PHP_METHOD(CairoContext, get_scaled_font)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_scaled_font: not yet implemented"); RETURN_FALSE;

	object_init(return_value)
}
/* }}} get_scaled_font */



/* {{{ proto object get_source()
   */
PHP_METHOD(CairoContext, get_source)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_source: not yet implemented"); RETURN_FALSE;

	object_init(return_value)
}
/* }}} get_source */



/* {{{ proto object get_target()
   */
PHP_METHOD(CairoContext, get_target)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_target: not yet implemented"); RETURN_FALSE;

	object_init(return_value)
}
/* }}} get_target */



/* {{{ proto float get_tolerance()
   */
PHP_METHOD(CairoContext, get_tolerance)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_tolerance: not yet implemented"); RETURN_FALSE;

	RETURN_DOUBLE(0.0);
}
/* }}} get_tolerance */



/* {{{ proto array get_extents(object obj[,int num])
   */
PHP_METHOD(CairoContext, glyph_extents)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	zval * obj = NULL;
	long num = 0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Oo|l", &_this_zval, CairoContext_ce_ptr, &obj, &num) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	array_init(return_value);

	do {
		/*This might be a prob*/
	} while (0);
}
/* }}} glyph_extents */



/* {{{ proto void glyph_pat(object obh[,int num])
   */
PHP_METHOD(CairoContext, glyph_path)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	zval * obh = NULL;
	long num = 0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Oo|l", &_this_zval, CairoContext_ce_ptr, &obh, &num) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "glyph_path: not yet implemented"); RETURN_FALSE;

}
/* }}} glyph_path */



/* {{{ proto bool has_current_point()
   */
PHP_METHOD(CairoContext, has_current_point)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	do {
		/* ONLY for CAIRO 1.6 */
	} while (0);
}
/* }}} has_current_point */



/* {{{ proto void identity_matrix()
   */
PHP_METHOD(CairoContext, identity_matrix)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "identity_matrix: not yet implemented"); RETURN_FALSE;

}
/* }}} identity_matrix */



/* {{{ proto bool in_fill(float x, float y)
   */
PHP_METHOD(CairoContext, in_fill)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double x = 0.0;
	double y = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Odd", &_this_zval, CairoContext_ce_ptr, &x, &y) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "in_fill: not yet implemented"); RETURN_FALSE;

	RETURN_FALSE;
}
/* }}} in_fill */



/* {{{ proto bool in_stroke(float x, float y)
   */
PHP_METHOD(CairoContext, in_stroke)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double x = 0.0;
	double y = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Odd", &_this_zval, CairoContext_ce_ptr, &x, &y) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "in_stroke: not yet implemented"); RETURN_FALSE;

	RETURN_FALSE;
}
/* }}} in_stroke */



/* {{{ proto void line_to(float x, float y)
   */
PHP_METHOD(CairoContext, line_to)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double x = 0.0;
	double y = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Odd", &_this_zval, CairoContext_ce_ptr, &x, &y) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "line_to: not yet implemented"); RETURN_FALSE;

}
/* }}} line_to */



/* {{{ proto void mask(object p)
   */
PHP_METHOD(CairoContext, mask)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	zval * p = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Oo", &_this_zval, CairoContext_ce_ptr, &p) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "mask: not yet implemented"); RETURN_FALSE;

}
/* }}} mask */



/* {{{ proto void mask_surface(object s[,float surface_x, float surface_y])
   */
PHP_METHOD(CairoContext, mask_surface)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	zval * s = NULL;
	double surface_x = 0.0;
	double surface_y = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Oo|dd", &_this_zval, CairoContext_ce_ptr, &s, &surface_x, &surface_y) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "mask_surface: not yet implemented"); RETURN_FALSE;

}
/* }}} mask_surface */



/* {{{ proto void move_to(float x, float y)
   */
PHP_METHOD(CairoContext, move_to)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double x = 0.0;
	double y = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Odd", &_this_zval, CairoContext_ce_ptr, &x, &y) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "move_to: not yet implemented"); RETURN_FALSE;

}
/* }}} move_to */



/* {{{ proto void new_path()
   */
PHP_METHOD(CairoContext, new_path)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "new_path: not yet implemented"); RETURN_FALSE;

}
/* }}} new_path */



/* {{{ proto void new_sub_path()
   */
PHP_METHOD(CairoContext, new_sub_path)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "new_sub_path: not yet implemented"); RETURN_FALSE;

}
/* }}} new_sub_path */



/* {{{ proto void paint()
   */
PHP_METHOD(CairoContext, paint)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "paint: not yet implemented"); RETURN_FALSE;

}
/* }}} paint */



/* {{{ proto void paint_with_alpha(float alpha)
   */
PHP_METHOD(CairoContext, paint_with_alpha)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double alpha = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Od", &_this_zval, CairoContext_ce_ptr, &alpha) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "paint_with_alpha: not yet implemented"); RETURN_FALSE;

}
/* }}} paint_with_alpha */



/* {{{ proto array path_extents([object path])
   */
PHP_METHOD(CairoContext, path_extents)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	zval * path = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O|o", &_this_zval, CairoContext_ce_ptr, &path) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	array_init(return_value);

	do {
		/* ONLY for CAIRO 1.6 */
	} while (0);
}
/* }}} path_extents */



/* {{{ proto object pop_group()
   */
PHP_METHOD(CairoContext, pop_group)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "pop_group: not yet implemented"); RETURN_FALSE;

	object_init(return_value)
}
/* }}} pop_group */



/* {{{ proto void pop_group_to_source()
   */
PHP_METHOD(CairoContext, pop_group_to_source)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "pop_group_to_source: not yet implemented"); RETURN_FALSE;

}
/* }}} pop_group_to_source */



/* {{{ proto void push_group()
   */
PHP_METHOD(CairoContext, push_group)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "push_group: not yet implemented"); RETURN_FALSE;

}
/* }}} push_group */



/* {{{ proto void push_group_with_content(int content)
   */
PHP_METHOD(CairoContext, push_group_with_content)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	long content = 0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Ol", &_this_zval, CairoContext_ce_ptr, &content) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "push_group_with_content: not yet implemented"); RETURN_FALSE;

}
/* }}} push_group_with_content */



/* {{{ proto void rectangle(float x, float y, float width, float height)
   */
PHP_METHOD(CairoContext, rectangle)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double x = 0.0;
	double y = 0.0;
	double width = 0.0;
	double height = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Odddd", &_this_zval, CairoContext_ce_ptr, &x, &y, &width, &height) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "rectangle: not yet implemented"); RETURN_FALSE;

}
/* }}} rectangle */



/* {{{ proto void rel_curve_to(float x1, float y1, float x2, float y2, float x3, float y3)
   */
PHP_METHOD(CairoContext, rel_curve_to)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double x1 = 0.0;
	double y1 = 0.0;
	double x2 = 0.0;
	double y2 = 0.0;
	double x3 = 0.0;
	double y3 = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Odddddd", &_this_zval, CairoContext_ce_ptr, &x1, &y1, &x2, &y2, &x3, &y3) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "rel_curve_to: not yet implemented"); RETURN_FALSE;

}
/* }}} rel_curve_to */



/* {{{ proto void rel_line_to(float x, float y)
   */
PHP_METHOD(CairoContext, rel_line_to)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double x = 0.0;
	double y = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Odd", &_this_zval, CairoContext_ce_ptr, &x, &y) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "rel_line_to: not yet implemented"); RETURN_FALSE;

}
/* }}} rel_line_to */



/* {{{ proto void rel_move_to(float x, float y)
   */
PHP_METHOD(CairoContext, rel_move_to)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double x = 0.0;
	double y = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Odd", &_this_zval, CairoContext_ce_ptr, &x, &y) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "rel_move_to: not yet implemented"); RETURN_FALSE;

}
/* }}} rel_move_to */



/* {{{ proto void reset_clip()
   */
PHP_METHOD(CairoContext, reset_clip)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "reset_clip: not yet implemented"); RETURN_FALSE;

}
/* }}} reset_clip */



/* {{{ proto void restore()
   */
PHP_METHOD(CairoContext, restore)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "restore: not yet implemented"); RETURN_FALSE;

}
/* }}} restore */



/* {{{ proto void rotate(float angle)
   */
PHP_METHOD(CairoContext, rotate)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double angle = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Od", &_this_zval, CairoContext_ce_ptr, &angle) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "rotate: not yet implemented"); RETURN_FALSE;

}
/* }}} rotate */



/* {{{ proto void save()
   */
PHP_METHOD(CairoContext, save)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "save: not yet implemented"); RETURN_FALSE;

}
/* }}} save */



/* {{{ proto void scale(float x, float y)
   */
PHP_METHOD(CairoContext, scale)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double x = 0.0;
	double y = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Odd", &_this_zval, CairoContext_ce_ptr, &x, &y) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "scale: not yet implemented"); RETURN_FALSE;

}
/* }}} scale */



/* {{{ proto void select_font_face(object obj[, int slant, int weight])
   */
PHP_METHOD(CairoContext, select_font_face)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	zval * obj = NULL;
	long slant = 0;
	long weight = 0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Oo|ll", &_this_zval, CairoContext_ce_ptr, &obj, &slant, &weight) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "select_font_face: not yet implemented"); RETURN_FALSE;

}
/* }}} select_font_face */



/* {{{ proto void set_antialias([int antialias])
   */
PHP_METHOD(CairoContext, set_antialias)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	long antialias = 0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O|l", &_this_zval, CairoContext_ce_ptr, &antialias) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "set_antialias: not yet implemented"); RETURN_FALSE;

}
/* }}} set_antialias */



/* {{{ proto void set_dash(object dashes [,float offset])
   */
PHP_METHOD(CairoContext, set_dash)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	zval * dashes = NULL;
	double offset = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Oo|d", &_this_zval, CairoContext_ce_ptr, &dashes, &offset) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "set_dash: not yet implemented"); RETURN_FALSE;

}
/* }}} set_dash */



/* {{{ proto void set_fill_rule(int fill_rule)
   */
PHP_METHOD(CairoContext, set_fill_rule)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	long fill_rule = 0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Ol", &_this_zval, CairoContext_ce_ptr, &fill_rule) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "set_fill_rule: not yet implemented"); RETURN_FALSE;

}
/* }}} set_fill_rule */



/* {{{ proto void set_font_face([object obj])
   */
PHP_METHOD(CairoContext, set_font_face)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	zval * obj = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O|o", &_this_zval, CairoContext_ce_ptr, &obj) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "set_font_face: not yet implemented"); RETURN_FALSE;

}
/* }}} set_font_face */



/* {{{ proto void set_font_matrix(object matrix)
   */
PHP_METHOD(CairoContext, set_font_matrix)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	zval * matrix = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Oo", &_this_zval, CairoContext_ce_ptr, &matrix) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "set_font_matrix: not yet implemented"); RETURN_FALSE;

}
/* }}} set_font_matrix */



/* {{{ proto void set_font_options(object options)
   */
PHP_METHOD(CairoContext, set_font_options)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	zval * options = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Oo", &_this_zval, CairoContext_ce_ptr, &options) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "set_font_options: not yet implemented"); RETURN_FALSE;

}
/* }}} set_font_options */



/* {{{ proto void set_font_size(float size)
   */
PHP_METHOD(CairoContext, set_font_size)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double size = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Od", &_this_zval, CairoContext_ce_ptr, &size) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "set_font_size: not yet implemented"); RETURN_FALSE;

}
/* }}} set_font_size */



/* {{{ proto void set_line_cap(int line_cap)
   */
PHP_METHOD(CairoContext, set_line_cap)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	long line_cap = 0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Ol", &_this_zval, CairoContext_ce_ptr, &line_cap) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "set_line_cap: not yet implemented"); RETURN_FALSE;

}
/* }}} set_line_cap */



/* {{{ proto void set_line_join(int line_join)
   */
PHP_METHOD(CairoContext, set_line_join)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	long line_join = 0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Ol", &_this_zval, CairoContext_ce_ptr, &line_join) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "set_line_join: not yet implemented"); RETURN_FALSE;

}
/* }}} set_line_join */



/* {{{ proto void set_line_width(float width)
   */
PHP_METHOD(CairoContext, set_line_width)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double width = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Od", &_this_zval, CairoContext_ce_ptr, &width) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "set_line_width: not yet implemented"); RETURN_FALSE;

}
/* }}} set_line_width */



/* {{{ proto void set_matrix(object matix)
   */
PHP_METHOD(CairoContext, set_matrix)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	zval * matix = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Oo", &_this_zval, CairoContext_ce_ptr, &matix) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "set_matrix: not yet implemented"); RETURN_FALSE;

}
/* }}} set_matrix */



/* {{{ proto void set_miter_limit(float limit)
   */
PHP_METHOD(CairoContext, set_miter_limit)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double limit = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Od", &_this_zval, CairoContext_ce_ptr, &limit) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "set_miter_limit: not yet implemented"); RETURN_FALSE;

}
/* }}} set_miter_limit */



/* {{{ proto void set_operator(int op)
   */
PHP_METHOD(CairoContext, set_operator)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	long op = 0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Ol", &_this_zval, CairoContext_ce_ptr, &op) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "set_operator: not yet implemented"); RETURN_FALSE;

}
/* }}} set_operator */



/* {{{ proto void set_source(object p)
   */
PHP_METHOD(CairoContext, set_source)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	zval * p = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Oo", &_this_zval, CairoContext_ce_ptr, &p) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "set_source: not yet implemented"); RETURN_FALSE;

}
/* }}} set_source */



/* {{{ proto void set_source_rgb(float red, float green, float blue)
   */
PHP_METHOD(CairoContext, set_source_rgb)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double red = 0.0;
	double green = 0.0;
	double blue = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Oddd", &_this_zval, CairoContext_ce_ptr, &red, &green, &blue) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "set_source_rgb: not yet implemented"); RETURN_FALSE;

}
/* }}} set_source_rgb */



/* {{{ proto void set_source_rgba(float red, float green, float blue [,float alpha])
   */
PHP_METHOD(CairoContext, set_source_rgba)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double red = 0.0;
	double green = 0.0;
	double blue = 0.0;
	double alpha = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Oddd|d", &_this_zval, CairoContext_ce_ptr, &red, &green, &blue, &alpha) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "set_source_rgba: not yet implemented"); RETURN_FALSE;

}
/* }}} set_source_rgba */



/* {{{ proto void set_source_surface(object surface [,float x, float y])
   */
PHP_METHOD(CairoContext, set_source_surface)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	zval * surface = NULL;
	double x = 0.0;
	double y = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Oo|dd", &_this_zval, CairoContext_ce_ptr, &surface, &x, &y) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "set_source_surface: not yet implemented"); RETURN_FALSE;

}
/* }}} set_source_surface */



/* {{{ proto void set_tolerance(float tolerance)
   */
PHP_METHOD(CairoContext, set_tolerance)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double tolerance = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Od", &_this_zval, CairoContext_ce_ptr, &tolerance) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "set_tolerance: not yet implemented"); RETURN_FALSE;

}
/* }}} set_tolerance */



/* {{{ proto void show_glyphs(object glyphs [,int num_glyphs])
   */
PHP_METHOD(CairoContext, show_glyphs)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	zval * glyphs = NULL;
	long num_glyphs = 0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Oo|l", &_this_zval, CairoContext_ce_ptr, &glyphs, &num_glyphs) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "show_glyphs: not yet implemented"); RETURN_FALSE;

}
/* }}} show_glyphs */



/* {{{ proto void show_page()
   */
PHP_METHOD(CairoContext, show_page)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "show_page: not yet implemented"); RETURN_FALSE;

}
/* }}} show_page */



/* {{{ proto void show_text(object obj)
   */
PHP_METHOD(CairoContext, show_text)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	zval * obj = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Oo", &_this_zval, CairoContext_ce_ptr, &obj) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "show_text: not yet implemented"); RETURN_FALSE;

}
/* }}} show_text */



/* {{{ proto void stroke()
   */
PHP_METHOD(CairoContext, stroke)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "stroke: not yet implemented"); RETURN_FALSE;

}
/* }}} stroke */



/* {{{ proto array stroke_extents()
   */
PHP_METHOD(CairoContext, stroke_extents)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	array_init(return_value);

	php_error(E_WARNING, "stroke_extents: not yet implemented"); RETURN_FALSE;

	array_init(return_value);
}
/* }}} stroke_extents */



/* {{{ proto void stroke_preserve()
   */
PHP_METHOD(CairoContext, stroke_preserve)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoContext_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "stroke_preserve: not yet implemented"); RETURN_FALSE;

}
/* }}} stroke_preserve */



/* {{{ proto array text_extents(object extents)
   */
PHP_METHOD(CairoContext, text_extents)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	zval * extents = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Oo", &_this_zval, CairoContext_ce_ptr, &extents) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	array_init(return_value);

	php_error(E_WARNING, "text_extents: not yet implemented"); RETURN_FALSE;

	array_init(return_value);
}
/* }}} text_extents */



/* {{{ proto void text_path(object obj)
   */
PHP_METHOD(CairoContext, text_path)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	zval * obj = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Oo", &_this_zval, CairoContext_ce_ptr, &obj) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "text_path: not yet implemented"); RETURN_FALSE;

}
/* }}} text_path */



/* {{{ proto void transform(object matrix)
   */
PHP_METHOD(CairoContext, transform)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	zval * matrix = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Oo", &_this_zval, CairoContext_ce_ptr, &matrix) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "transform: not yet implemented"); RETURN_FALSE;

}
/* }}} transform */



/* {{{ proto void translate(float tx, float ty)
   */
PHP_METHOD(CairoContext, translate)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double tx = 0.0;
	double ty = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Odd", &_this_zval, CairoContext_ce_ptr, &tx, &ty) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "translate: not yet implemented"); RETURN_FALSE;

}
/* }}} translate */



/* {{{ proto array user_to_device(float x, float y)
   */
PHP_METHOD(CairoContext, user_to_device)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double x = 0.0;
	double y = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Odd", &_this_zval, CairoContext_ce_ptr, &x, &y) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	array_init(return_value);

	php_error(E_WARNING, "user_to_device: not yet implemented"); RETURN_FALSE;

	array_init(return_value);
}
/* }}} user_to_device */



/* {{{ proto array user_to_device_distance(float dx, float dy)
   */
PHP_METHOD(CairoContext, user_to_device_distance)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double dx = 0.0;
	double dy = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Odd", &_this_zval, CairoContext_ce_ptr, &dx, &dy) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	array_init(return_value);

	php_error(E_WARNING, "user_to_device_distance: not yet implemented"); RETURN_FALSE;

	array_init(return_value);
}
/* }}} user_to_device_distance */


static zend_function_entry CairoContext_methods[] = {
	PHP_ME(CairoContext, append_path, CairoContext__append_path_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, arc, CairoContext__arc_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, arc_negative, CairoContext__arc_negative_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, clip, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, clip_extents, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, clip_preserve, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, close_path, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, copy_clip_rectangle_list, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, copy_page, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, copy_path, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, copy_path_flat, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, curve_to, CairoContext__curve_to_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, device_to_user, CairoContext__device_to_user_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, device_to_user_distance, CairoContext__device_to_user_distance_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, fill, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, fill_extents, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, fill_preserve, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, font_extents, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, get_antialias, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, get_current_point, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, get_dash, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, get_dash_count, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, get_fill_rule, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, get_font_face, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, get_font_matrix, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, get_font_options, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, get_group_target, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, get_line_cap, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, get_line_join, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, get_line_width, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, get_matrix, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, get_matrix_limit, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, get_operator, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, get_scaled_font, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, get_source, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, get_target, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, get_tolerance, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, glyph_extents, CairoContext__glyph_extents_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, glyph_path, CairoContext__glyph_path_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, has_current_point, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, identity_matrix, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, in_fill, CairoContext__in_fill_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, in_stroke, CairoContext__in_stroke_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, line_to, CairoContext__line_to_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, mask, CairoContext__mask_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, mask_surface, CairoContext__mask_surface_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, move_to, CairoContext__move_to_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, new_path, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, new_sub_path, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, paint, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, paint_with_alpha, CairoContext__paint_with_alpha_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, path_extents, CairoContext__path_extents_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, pop_group, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, pop_group_to_source, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, push_group, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, push_group_with_content, CairoContext__push_group_with_content_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, rectangle, CairoContext__rectangle_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, rel_curve_to, CairoContext__rel_curve_to_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, rel_line_to, CairoContext__rel_line_to_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, rel_move_to, CairoContext__rel_move_to_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, reset_clip, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, restore, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, rotate, CairoContext__rotate_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, save, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, scale, CairoContext__scale_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, select_font_face, CairoContext__select_font_face_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, set_antialias, CairoContext__set_antialias_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, set_dash, CairoContext__set_dash_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, set_fill_rule, CairoContext__set_fill_rule_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, set_font_face, CairoContext__set_font_face_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, set_font_matrix, CairoContext__set_font_matrix_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, set_font_options, CairoContext__set_font_options_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, set_font_size, CairoContext__set_font_size_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, set_line_cap, CairoContext__set_line_cap_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, set_line_join, CairoContext__set_line_join_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, set_line_width, CairoContext__set_line_width_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, set_matrix, CairoContext__set_matrix_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, set_miter_limit, CairoContext__set_miter_limit_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, set_operator, CairoContext__set_operator_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, set_source, CairoContext__set_source_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, set_source_rgb, CairoContext__set_source_rgb_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, set_source_rgba, CairoContext__set_source_rgba_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, set_source_surface, CairoContext__set_source_surface_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, set_tolerance, CairoContext__set_tolerance_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, show_glyphs, CairoContext__show_glyphs_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, show_page, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, show_text, CairoContext__show_text_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, stroke, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, stroke_extents, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, stroke_preserve, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, text_extents, CairoContext__text_extents_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, text_path, CairoContext__text_path_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, transform, CairoContext__transform_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, translate, CairoContext__translate_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, user_to_device, CairoContext__user_to_device_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoContext, user_to_device_distance, CairoContext__user_to_device_distance_args, /**/ZEND_ACC_PUBLIC)
	{ NULL, NULL, NULL }
};

/* }}} Methods */

static void class_init_CairoContext(void)
{
	zend_class_entry ce;

	INIT_CLASS_ENTRY(ce, "CairoContext", CairoContext_methods);
	CairoContext_ce_ptr = zend_register_internal_class(&ce);
}

/* }}} Class CairoContext */

/* {{{ Class CairoFontFace */

static zend_class_entry * CairoFontFace_ce_ptr = NULL;

/* {{{ Methods */


/* {{{ proto void construct()
   */
PHP_METHOD(CairoFontFace, __construct)
{
	zend_class_entry * _this_ce;
	zval * _this_zval;



	if (ZEND_NUM_ARGS()>0)  {
		WRONG_PARAM_COUNT;
	}


	do {
		/* not allowede to create an object directly*/
	} while (0);
}
/* }}} __construct */



/* {{{ proto void destruct()
   */
PHP_METHOD(CairoFontFace, __destruct)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoFontFace_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "__destruct: not yet implemented"); RETURN_FALSE;

}
/* }}} __destruct */


static zend_function_entry CairoFontFace_methods[] = {
	PHP_ME(CairoFontFace, __construct, NULL, /**/ZEND_ACC_PUBLIC | ZEND_ACC_CTOR)
	PHP_ME(CairoFontFace, __destruct, NULL, /**/ZEND_ACC_PUBLIC)
	{ NULL, NULL, NULL }
};

/* }}} Methods */

static void class_init_CairoFontFace(void)
{
	zend_class_entry ce;

	INIT_CLASS_ENTRY(ce, "CairoFontFace", CairoFontFace_methods);
	CairoFontFace_ce_ptr = zend_register_internal_class(&ce);
}

/* }}} Class CairoFontFace */

/* {{{ Class CairoFontOptions */

static zend_class_entry * CairoFontOptions_ce_ptr = NULL;

/* {{{ Methods */


/* {{{ proto void construct()
   */
PHP_METHOD(CairoFontOptions, __construct)
{
	zend_class_entry * _this_ce;
	zval * _this_zval;



	if (ZEND_NUM_ARGS()>0)  {
		WRONG_PARAM_COUNT;
	}


	php_error(E_WARNING, "__construct: not yet implemented"); RETURN_FALSE;

}
/* }}} __construct */



/* {{{ proto int get_antialias()
   */
PHP_METHOD(CairoFontOptions, get_antialias)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoFontOptions_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_antialias: not yet implemented"); RETURN_FALSE;

	RETURN_LONG(0);
}
/* }}} get_antialias */



/* {{{ proto int get_hint_metrics()
   */
PHP_METHOD(CairoFontOptions, get_hint_metrics)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoFontOptions_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_hint_metrics: not yet implemented"); RETURN_FALSE;

	RETURN_LONG(0);
}
/* }}} get_hint_metrics */



/* {{{ proto int get_hint_style()
   */
PHP_METHOD(CairoFontOptions, get_hint_style)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoFontOptions_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_hint_style: not yet implemented"); RETURN_FALSE;

	RETURN_LONG(0);
}
/* }}} get_hint_style */



/* {{{ proto int get_subpixel_order()
   */
PHP_METHOD(CairoFontOptions, get_subpixel_order)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoFontOptions_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_subpixel_order: not yet implemented"); RETURN_FALSE;

	RETURN_LONG(0);
}
/* }}} get_subpixel_order */



/* {{{ proto void set_antialias([int aa])
   */
PHP_METHOD(CairoFontOptions, set_antialias)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	long aa = 0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O|l", &_this_zval, CairoFontOptions_ce_ptr, &aa) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "set_antialias: not yet implemented"); RETURN_FALSE;

}
/* }}} set_antialias */



/* {{{ proto void set_hint_metrics([int hm])
   */
PHP_METHOD(CairoFontOptions, set_hint_metrics)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	long hm = 0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O|l", &_this_zval, CairoFontOptions_ce_ptr, &hm) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "set_hint_metrics: not yet implemented"); RETURN_FALSE;

}
/* }}} set_hint_metrics */



/* {{{ proto void set_hint_style([int hs])
   */
PHP_METHOD(CairoFontOptions, set_hint_style)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	long hs = 0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O|l", &_this_zval, CairoFontOptions_ce_ptr, &hs) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "set_hint_style: not yet implemented"); RETURN_FALSE;

}
/* }}} set_hint_style */



/* {{{ proto void set_subpixel_order([int so])
   */
PHP_METHOD(CairoFontOptions, set_subpixel_order)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	long so = 0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O|l", &_this_zval, CairoFontOptions_ce_ptr, &so) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "set_subpixel_order: not yet implemented"); RETURN_FALSE;

}
/* }}} set_subpixel_order */


static zend_function_entry CairoFontOptions_methods[] = {
	PHP_ME(CairoFontOptions, __construct, NULL, /**/ZEND_ACC_PUBLIC | ZEND_ACC_CTOR)
	PHP_ME(CairoFontOptions, get_antialias, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoFontOptions, get_hint_metrics, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoFontOptions, get_hint_style, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoFontOptions, get_subpixel_order, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoFontOptions, set_antialias, CairoFontOptions__set_antialias_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoFontOptions, set_hint_metrics, CairoFontOptions__set_hint_metrics_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoFontOptions, set_hint_style, CairoFontOptions__set_hint_style_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoFontOptions, set_subpixel_order, CairoFontOptions__set_subpixel_order_args, /**/ZEND_ACC_PUBLIC)
	{ NULL, NULL, NULL }
};

/* }}} Methods */

static void class_init_CairoFontOptions(void)
{
	zend_class_entry ce;

	INIT_CLASS_ENTRY(ce, "CairoFontOptions", CairoFontOptions_methods);
	CairoFontOptions_ce_ptr = zend_register_internal_class(&ce);
}

/* }}} Class CairoFontOptions */

/* {{{ Class CairoMatrix */

static zend_class_entry * CairoMatrix_ce_ptr = NULL;

/* {{{ Methods */


/* {{{ proto void construct([float xx, float yx, float xy, float yy, float x0, float y0])
   */
PHP_METHOD(CairoMatrix, __construct)
{
	zend_class_entry * _this_ce;
	zval * _this_zval;

	double xx = 0.0;
	double yx = 0.0;
	double xy = 0.0;
	double yy = 0.0;
	double x0 = 0.0;
	double y0 = 0.0;



	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "|dddddd", &xx, &yx, &xy, &yy, &x0, &y0) == FAILURE) {
		return;
	}

	_this_zval = getThis();
	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "__construct: not yet implemented"); RETURN_FALSE;

}
/* }}} __construct */



/* {{{ proto object inti_rotate(float radians)
   */
PHP_METHOD(CairoMatrix, init_rotate)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double radians = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Od", &_this_zval, CairoMatrix_ce_ptr, &radians) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "init_rotate: not yet implemented"); RETURN_FALSE;

	object_init(return_value)
}
/* }}} init_rotate */



/* {{{ proto void invert()
   */
PHP_METHOD(CairoMatrix, invert)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoMatrix_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "invert: not yet implemented"); RETURN_FALSE;

}
/* }}} invert */



/* {{{ proto object multiply(object o2)
   */
PHP_METHOD(CairoMatrix, multiply)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	zval * o2 = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Oo", &_this_zval, CairoMatrix_ce_ptr, &o2) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "multiply: not yet implemented"); RETURN_FALSE;

	object_init(return_value)
}
/* }}} multiply */



/* {{{ proto void rotate(float radians)
   */
PHP_METHOD(CairoMatrix, rotate)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double radians = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Od", &_this_zval, CairoMatrix_ce_ptr, &radians) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "rotate: not yet implemented"); RETURN_FALSE;

}
/* }}} rotate */



/* {{{ proto void scale(float sx, float xy)
   */
PHP_METHOD(CairoMatrix, scale)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double sx = 0.0;
	double xy = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Odd", &_this_zval, CairoMatrix_ce_ptr, &sx, &xy) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "scale: not yet implemented"); RETURN_FALSE;

}
/* }}} scale */



/* {{{ proto array transform_distance(float dx, float dy)
   */
PHP_METHOD(CairoMatrix, transform_distance)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double dx = 0.0;
	double dy = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Odd", &_this_zval, CairoMatrix_ce_ptr, &dx, &dy) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	array_init(return_value);

	php_error(E_WARNING, "transform_distance: not yet implemented"); RETURN_FALSE;

	array_init(return_value);
}
/* }}} transform_distance */



/* {{{ proto array transform_point(float x, float y)
   */
PHP_METHOD(CairoMatrix, transform_point)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double x = 0.0;
	double y = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Odd", &_this_zval, CairoMatrix_ce_ptr, &x, &y) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	array_init(return_value);

	php_error(E_WARNING, "transform_point: not yet implemented"); RETURN_FALSE;

	array_init(return_value);
}
/* }}} transform_point */



/* {{{ proto void translate(float tx, float ty)
   */
PHP_METHOD(CairoMatrix, translate)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double tx = 0.0;
	double ty = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Odd", &_this_zval, CairoMatrix_ce_ptr, &tx, &ty) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "translate: not yet implemented"); RETURN_FALSE;

}
/* }}} translate */


static zend_function_entry CairoMatrix_methods[] = {
	PHP_ME(CairoMatrix, __construct, CairoMatrix____construct_args, /**/ZEND_ACC_PUBLIC | ZEND_ACC_CTOR)
	PHP_ME(CairoMatrix, init_rotate, CairoMatrix__init_rotate_args, /**/ZEND_ACC_PRIVATE)
	PHP_ME(CairoMatrix, invert, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoMatrix, multiply, CairoMatrix__multiply_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoMatrix, rotate, CairoMatrix__rotate_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoMatrix, scale, CairoMatrix__scale_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoMatrix, transform_distance, CairoMatrix__transform_distance_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoMatrix, transform_point, CairoMatrix__transform_point_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoMatrix, translate, CairoMatrix__translate_args, /**/ZEND_ACC_PUBLIC)
	{ NULL, NULL, NULL }
};

/* }}} Methods */

static void class_init_CairoMatrix(void)
{
	zend_class_entry ce;

	INIT_CLASS_ENTRY(ce, "CairoMatrix", CairoMatrix_methods);
	CairoMatrix_ce_ptr = zend_register_internal_class(&ce);
}

/* }}} Class CairoMatrix */

/* {{{ Class CairoPattern */

static zend_class_entry * CairoPattern_ce_ptr = NULL;

/* {{{ Methods */


/* {{{ proto void contruct()
   */
PHP_METHOD(CairoPattern, __construct)
{
	zend_class_entry * _this_ce;
	zval * _this_zval;



	if (ZEND_NUM_ARGS()>0)  {
		WRONG_PARAM_COUNT;
	}


	php_error(E_WARNING, "__construct: not yet implemented"); RETURN_FALSE;

}
/* }}} __construct */



/* {{{ proto object get_matrix()
   */
PHP_METHOD(CairoPattern, get_matrix)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoPattern_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_matrix: not yet implemented"); RETURN_FALSE;

	object_init(return_value)
}
/* }}} get_matrix */



/* {{{ proto void set_matrix(object m)
   */
PHP_METHOD(CairoPattern, set_matrix)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	zval * m = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Oo", &_this_zval, CairoPattern_ce_ptr, &m) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "set_matrix: not yet implemented"); RETURN_FALSE;

}
/* }}} set_matrix */


static zend_function_entry CairoPattern_methods[] = {
	PHP_ME(CairoPattern, __construct, NULL, /**/ZEND_ACC_PUBLIC | ZEND_ACC_CTOR)
	PHP_ME(CairoPattern, get_matrix, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoPattern, set_matrix, CairoPattern__set_matrix_args, /**/ZEND_ACC_PUBLIC)
	{ NULL, NULL, NULL }
};

/* }}} Methods */

static void class_init_CairoPattern(void)
{
	zend_class_entry ce;

	INIT_CLASS_ENTRY(ce, "CairoPattern", CairoPattern_methods);
	CairoPattern_ce_ptr = zend_register_internal_class(&ce);
}

/* }}} Class CairoPattern */

/* {{{ Class CairoGradient */

static zend_class_entry * CairoGradient_ce_ptr = NULL;

/* {{{ Methods */


/* {{{ proto void construct()
   */
PHP_METHOD(CairoGradient, __contruct)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoGradient_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "__contruct: not yet implemented"); RETURN_FALSE;

}
/* }}} __contruct */



/* {{{ proto void add_color_stop_rgb(float offset, float red, float green, float blue)
   */
PHP_METHOD(CairoGradient, add_color_stop_rgb)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double offset = 0.0;
	double red = 0.0;
	double green = 0.0;
	double blue = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Odddd", &_this_zval, CairoGradient_ce_ptr, &offset, &red, &green, &blue) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "add_color_stop_rgb: not yet implemented"); RETURN_FALSE;

}
/* }}} add_color_stop_rgb */



/* {{{ proto void add_color_stop_rgba(float offset, float red, float green, float blue, float alpha)
   */
PHP_METHOD(CairoGradient, add_color_stop_rgba)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double offset = 0.0;
	double red = 0.0;
	double green = 0.0;
	double blue = 0.0;
	double alpha = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Oddddd", &_this_zval, CairoGradient_ce_ptr, &offset, &red, &green, &blue, &alpha) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "add_color_stop_rgba: not yet implemented"); RETURN_FALSE;

}
/* }}} add_color_stop_rgba */


static zend_function_entry CairoGradient_methods[] = {
	PHP_ME(CairoGradient, __contruct, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoGradient, add_color_stop_rgb, CairoGradient__add_color_stop_rgb_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoGradient, add_color_stop_rgba, CairoGradient__add_color_stop_rgba_args, /**/ZEND_ACC_PUBLIC)
	{ NULL, NULL, NULL }
};

/* }}} Methods */

static void class_init_CairoGradient(void)
{
	zend_class_entry ce;

	INIT_CLASS_ENTRY(ce, "CairoGradient", CairoGradient_methods);
	CairoGradient_ce_ptr = zend_register_internal_class_ex(&ce, NULL, "CairoPattern" TSRMLS_CC);
}

/* }}} Class CairoGradient */

/* {{{ Class CairoLinearGradient */

static zend_class_entry * CairoLinearGradient_ce_ptr = NULL;

/* {{{ Methods */


/* {{{ proto void construct(float x0, float y0, float x1, float y1)
   */
PHP_METHOD(CairoLinearGradient, __construct)
{
	zend_class_entry * _this_ce;
	zval * _this_zval;

	double x0 = 0.0;
	double y0 = 0.0;
	double x1 = 0.0;
	double y1 = 0.0;



	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "dddd", &x0, &y0, &x1, &y1) == FAILURE) {
		return;
	}

	_this_zval = getThis();
	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "__construct: not yet implemented"); RETURN_FALSE;

}
/* }}} __construct */



/* {{{ proto array get_linear_points()
   */
PHP_METHOD(CairoLinearGradient, get_linear_points)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoLinearGradient_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	array_init(return_value);

	php_error(E_WARNING, "get_linear_points: not yet implemented"); RETURN_FALSE;

	array_init(return_value);
}
/* }}} get_linear_points */


static zend_function_entry CairoLinearGradient_methods[] = {
	PHP_ME(CairoLinearGradient, __construct, CairoLinearGradient____construct_args, /**/ZEND_ACC_PUBLIC | ZEND_ACC_CTOR)
	PHP_ME(CairoLinearGradient, get_linear_points, NULL, /**/ZEND_ACC_PUBLIC)
	{ NULL, NULL, NULL }
};

/* }}} Methods */

static void class_init_CairoLinearGradient(void)
{
	zend_class_entry ce;

	INIT_CLASS_ENTRY(ce, "CairoLinearGradient", CairoLinearGradient_methods);
	CairoLinearGradient_ce_ptr = zend_register_internal_class_ex(&ce, NULL, "CairoGradient" TSRMLS_CC);
}

/* }}} Class CairoLinearGradient */

/* {{{ Class CairoRadialGradient */

static zend_class_entry * CairoRadialGradient_ce_ptr = NULL;

/* {{{ Methods */


/* {{{ proto void construct(float cx0, float cy0, float radius0, float cx1, float cy1, float radius1)
   */
PHP_METHOD(CairoRadialGradient, __constuct)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double cx0 = 0.0;
	double cy0 = 0.0;
	double radius0 = 0.0;
	double cx1 = 0.0;
	double cy1 = 0.0;
	double radius1 = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Odddddd", &_this_zval, CairoRadialGradient_ce_ptr, &cx0, &cy0, &radius0, &cx1, &cy1, &radius1) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "__constuct: not yet implemented"); RETURN_FALSE;

}
/* }}} __constuct */



/* {{{ proto array get_radial_circles()
   */
PHP_METHOD(CairoRadialGradient, get_radial_circles)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoRadialGradient_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	array_init(return_value);

	php_error(E_WARNING, "get_radial_circles: not yet implemented"); RETURN_FALSE;

	array_init(return_value);
}
/* }}} get_radial_circles */


static zend_function_entry CairoRadialGradient_methods[] = {
	PHP_ME(CairoRadialGradient, __constuct, CairoRadialGradient____constuct_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoRadialGradient, get_radial_circles, NULL, /**/ZEND_ACC_PUBLIC)
	{ NULL, NULL, NULL }
};

/* }}} Methods */

static void class_init_CairoRadialGradient(void)
{
	zend_class_entry ce;

	INIT_CLASS_ENTRY(ce, "CairoRadialGradient", CairoRadialGradient_methods);
	CairoRadialGradient_ce_ptr = zend_register_internal_class_ex(&ce, NULL, "CairoGradient" TSRMLS_CC);
}

/* }}} Class CairoRadialGradient */

/* {{{ Class CairoSolidPattern */

static zend_class_entry * CairoSolidPattern_ce_ptr = NULL;

/* {{{ Methods */


/* {{{ proto void construct(float r, float g, float b [, float a])
   */
PHP_METHOD(CairoSolidPattern, __construct)
{
	zend_class_entry * _this_ce;
	zval * _this_zval;

	double r = 0.0;
	double g = 0.0;
	double b = 0.0;
	double a = 0.0;



	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "ddd|d", &r, &g, &b, &a) == FAILURE) {
		return;
	}

	_this_zval = getThis();
	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "__construct: not yet implemented"); RETURN_FALSE;

}
/* }}} __construct */



/* {{{ proto array get_rgba()
   */
PHP_METHOD(CairoSolidPattern, get_rgba)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoSolidPattern_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	array_init(return_value);

	php_error(E_WARNING, "get_rgba: not yet implemented"); RETURN_FALSE;

	array_init(return_value);
}
/* }}} get_rgba */


static zend_function_entry CairoSolidPattern_methods[] = {
	PHP_ME(CairoSolidPattern, __construct, CairoSolidPattern____construct_args, /**/ZEND_ACC_PUBLIC | ZEND_ACC_CTOR)
	PHP_ME(CairoSolidPattern, get_rgba, NULL, /**/ZEND_ACC_PUBLIC)
	{ NULL, NULL, NULL }
};

/* }}} Methods */

static void class_init_CairoSolidPattern(void)
{
	zend_class_entry ce;

	INIT_CLASS_ENTRY(ce, "CairoSolidPattern", CairoSolidPattern_methods);
	CairoSolidPattern_ce_ptr = zend_register_internal_class_ex(&ce, NULL, "CairoPattern" TSRMLS_CC);
}

/* }}} Class CairoSolidPattern */

/* {{{ Class CairoSurfacePattern */

static zend_class_entry * CairoSurfacePattern_ce_ptr = NULL;

/* {{{ Methods */


/* {{{ proto void construct(object s)
   */
PHP_METHOD(CairoSurfacePattern, __construct)
{
	zend_class_entry * _this_ce;
	zval * _this_zval;

	zval * s = NULL;



	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "o", &s) == FAILURE) {
		return;
	}

	_this_zval = getThis();
	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "__construct: not yet implemented"); RETURN_FALSE;

}
/* }}} __construct */



/* {{{ proto int get_extend()
   */
PHP_METHOD(CairoSurfacePattern, get_extend)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoSurfacePattern_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_extend: not yet implemented"); RETURN_FALSE;

	RETURN_LONG(0);
}
/* }}} get_extend */



/* {{{ proto int get_filter()
   */
PHP_METHOD(CairoSurfacePattern, get_filter)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoSurfacePattern_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_filter: not yet implemented"); RETURN_FALSE;

	RETURN_LONG(0);
}
/* }}} get_filter */



/* {{{ proto object get_surface()
   */
PHP_METHOD(CairoSurfacePattern, get_surface)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoSurfacePattern_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_surface: not yet implemented"); RETURN_FALSE;

	object_init(return_value)
}
/* }}} get_surface */



/* {{{ proto void set_extend(int extend)
   */
PHP_METHOD(CairoSurfacePattern, set_extend)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	long extend = 0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Ol", &_this_zval, CairoSurfacePattern_ce_ptr, &extend) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "set_extend: not yet implemented"); RETURN_FALSE;

}
/* }}} set_extend */



/* {{{ proto void set_filter(int filter)
   */
PHP_METHOD(CairoSurfacePattern, set_filter)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	long filter = 0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Ol", &_this_zval, CairoSurfacePattern_ce_ptr, &filter) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "set_filter: not yet implemented"); RETURN_FALSE;

}
/* }}} set_filter */


static zend_function_entry CairoSurfacePattern_methods[] = {
	PHP_ME(CairoSurfacePattern, __construct, NULL, /**/ZEND_ACC_PUBLIC | ZEND_ACC_CTOR)
	PHP_ME(CairoSurfacePattern, get_extend, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoSurfacePattern, get_filter, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoSurfacePattern, get_surface, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoSurfacePattern, set_extend, CairoSurfacePattern__set_extend_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoSurfacePattern, set_filter, CairoSurfacePattern__set_filter_args, /**/ZEND_ACC_PUBLIC)
	{ NULL, NULL, NULL }
};

/* }}} Methods */

static void class_init_CairoSurfacePattern(void)
{
	zend_class_entry ce;

	INIT_CLASS_ENTRY(ce, "CairoSurfacePattern", CairoSurfacePattern_methods);
	CairoSurfacePattern_ce_ptr = zend_register_internal_class_ex(&ce, NULL, "CairoPattern" TSRMLS_CC);
}

/* }}} Class CairoSurfacePattern */

/* {{{ Class CairoScaledFont */

static zend_class_entry * CairoScaledFont_ce_ptr = NULL;

/* {{{ Methods */


/* {{{ proto void construct(object ff, object mx1, object mx2, object fo)
   */
PHP_METHOD(CairoScaledFont, __construct)
{
	zend_class_entry * _this_ce;
	zval * _this_zval;

	zval * ff = NULL;
	zval * mx1 = NULL;
	zval * mx2 = NULL;
	zval * fo = NULL;



	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "oooo", &ff, &mx1, &mx2, &fo) == FAILURE) {
		return;
	}

	_this_zval = getThis();
	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "__construct: not yet implemented"); RETURN_FALSE;

}
/* }}} __construct */



/* {{{ proto array extents()
   */
PHP_METHOD(CairoScaledFont, extents)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoScaledFont_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	array_init(return_value);

	php_error(E_WARNING, "extents: not yet implemented"); RETURN_FALSE;

	array_init(return_value);
}
/* }}} extents */



/* {{{ proto object get_font_face()
   */
PHP_METHOD(CairoScaledFont, get_font_face)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoScaledFont_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_font_face: not yet implemented"); RETURN_FALSE;

	object_init(return_value)
}
/* }}} get_font_face */



/* {{{ proto array text_extents(string str)
   */
PHP_METHOD(CairoScaledFont, text_extents)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	const char * str = NULL;
	int str_len = 0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Os", &_this_zval, CairoScaledFont_ce_ptr, &str, &str_len) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	array_init(return_value);

	php_error(E_WARNING, "text_extents: not yet implemented"); RETURN_FALSE;

	array_init(return_value);
}
/* }}} text_extents */


static zend_function_entry CairoScaledFont_methods[] = {
	PHP_ME(CairoScaledFont, __construct, CairoScaledFont____construct_args, /**/ZEND_ACC_PUBLIC | ZEND_ACC_CTOR)
	PHP_ME(CairoScaledFont, extents, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoScaledFont, get_font_face, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoScaledFont, text_extents, CairoScaledFont__text_extents_args, /**/ZEND_ACC_PUBLIC)
	{ NULL, NULL, NULL }
};

/* }}} Methods */

static void class_init_CairoScaledFont(void)
{
	zend_class_entry ce;

	INIT_CLASS_ENTRY(ce, "CairoScaledFont", CairoScaledFont_methods);
	CairoScaledFont_ce_ptr = zend_register_internal_class(&ce);
}

/* }}} Class CairoScaledFont */

/* {{{ Class CairoSurface */

static zend_class_entry * CairoSurface_ce_ptr = NULL;

/* {{{ Methods */


/* {{{ proto void construct()
   */
PHP_METHOD(CairoSurface, __construct)
{
	zend_class_entry * _this_ce;
	zval * _this_zval;



	if (ZEND_NUM_ARGS()>0)  {
		WRONG_PARAM_COUNT;
	}


	php_error(E_WARNING, "__construct: not yet implemented"); RETURN_FALSE;

}
/* }}} __construct */



/* {{{ proto object create_similar(int content, int width, int height)
   */
PHP_METHOD(CairoSurface, create_similar)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	long content = 0;
	long width = 0;
	long height = 0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Olll", &_this_zval, CairoSurface_ce_ptr, &content, &width, &height) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "create_similar: not yet implemented"); RETURN_FALSE;

	object_init(return_value)
}
/* }}} create_similar */



/* {{{ proto void finish()
   */
PHP_METHOD(CairoSurface, finish)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoSurface_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "finish: not yet implemented"); RETURN_FALSE;

}
/* }}} finish */



/* {{{ proto void flush()
   */
PHP_METHOD(CairoSurface, flush)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoSurface_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "flush: not yet implemented"); RETURN_FALSE;

}
/* }}} flush */



/* {{{ proto int get_content()
   */
PHP_METHOD(CairoSurface, get_content)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoSurface_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_content: not yet implemented"); RETURN_FALSE;

	RETURN_LONG(0);
}
/* }}} get_content */



/* {{{ proto array get_device_offset()
   */
PHP_METHOD(CairoSurface, get_device_offset)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoSurface_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	array_init(return_value);

	php_error(E_WARNING, "get_device_offset: not yet implemented"); RETURN_FALSE;

	array_init(return_value);
}
/* }}} get_device_offset */



/* {{{ proto object get_font_options()
   */
PHP_METHOD(CairoSurface, get_font_options)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoSurface_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_font_options: not yet implemented"); RETURN_FALSE;

	object_init(return_value)
}
/* }}} get_font_options */



/* {{{ proto void mark_dirty_rectangle([int x, int y, int width, int height])
   */
PHP_METHOD(CairoSurface, mark_dirty_rectangle)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	long x = 0;
	long y = 0;
	long width = 0;
	long height = 0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O|llll", &_this_zval, CairoSurface_ce_ptr, &x, &y, &width, &height) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "mark_dirty_rectangle: not yet implemented"); RETURN_FALSE;

}
/* }}} mark_dirty_rectangle */



/* {{{ proto void set_device_offset(float x_offset, float y_offset)
   */
PHP_METHOD(CairoSurface, set_device_offset)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double x_offset = 0.0;
	double y_offset = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Odd", &_this_zval, CairoSurface_ce_ptr, &x_offset, &y_offset) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "set_device_offset: not yet implemented"); RETURN_FALSE;

}
/* }}} set_device_offset */



/* {{{ proto void set_fallback_resolution(float x_ppi, float y_ppi)
   */
PHP_METHOD(CairoSurface, set_fallback_resolution)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double x_ppi = 0.0;
	double y_ppi = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Odd", &_this_zval, CairoSurface_ce_ptr, &x_ppi, &y_ppi) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "set_fallback_resolution: not yet implemented"); RETURN_FALSE;

}
/* }}} set_fallback_resolution */



/* {{{ proto void write_to_png(string file)
   */
PHP_METHOD(CairoSurface, write_to_png)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	const char * file = NULL;
	int file_len = 0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Os", &_this_zval, CairoSurface_ce_ptr, &file, &file_len) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "write_to_png: not yet implemented"); RETURN_FALSE;

}
/* }}} write_to_png */


static zend_function_entry CairoSurface_methods[] = {
	PHP_ME(CairoSurface, __construct, NULL, /**/ZEND_ACC_PUBLIC | ZEND_ACC_CTOR)
	PHP_ME(CairoSurface, create_similar, CairoSurface__create_similar_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoSurface, finish, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoSurface, flush, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoSurface, get_content, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoSurface, get_device_offset, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoSurface, get_font_options, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoSurface, mark_dirty_rectangle, CairoSurface__mark_dirty_rectangle_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoSurface, set_device_offset, CairoSurface__set_device_offset_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoSurface, set_fallback_resolution, CairoSurface__set_fallback_resolution_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoSurface, write_to_png, CairoSurface__write_to_png_args, /**/ZEND_ACC_PUBLIC)
	{ NULL, NULL, NULL }
};

/* }}} Methods */

static void class_init_CairoSurface(void)
{
	zend_class_entry ce;

	INIT_CLASS_ENTRY(ce, "CairoSurface", CairoSurface_methods);
	CairoSurface_ce_ptr = zend_register_internal_class(&ce);
}

/* }}} Class CairoSurface */

/* {{{ Class CairoImageSurface */

static zend_class_entry * CairoImageSurface_ce_ptr = NULL;

/* {{{ Methods */


/* {{{ proto void construct(int format, int widthm, int height)
   */
PHP_METHOD(CairoImageSurface, __construct)
{
	zend_class_entry * _this_ce;
	zval * _this_zval;

	long format = 0;
	long widthm = 0;
	long height = 0;



	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "lll", &format, &widthm, &height) == FAILURE) {
		return;
	}

	_this_zval = getThis();
	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "__construct: not yet implemented"); RETURN_FALSE;

}
/* }}} __construct */



/* {{{ proto object create_from_data(object obj, int format, int width, int height [, int stride])
   */
PHP_METHOD(CairoImageSurface, create_from_data)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	zval * obj = NULL;
	long format = 0;
	long width = 0;
	long height = 0;
	long stride = 0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Oolll|l", &_this_zval, CairoImageSurface_ce_ptr, &obj, &format, &width, &height, &stride) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "create_from_data: not yet implemented"); RETURN_FALSE;

	object_init(return_value)
}
/* }}} create_from_data */



/* {{{ proto object create_from_png(string file)
   */
PHP_METHOD(CairoImageSurface, create_from_png)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	const char * file = NULL;
	int file_len = 0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Os", &_this_zval, CairoImageSurface_ce_ptr, &file, &file_len) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "create_from_png: not yet implemented"); RETURN_FALSE;

	object_init(return_value)
}
/* }}} create_from_png */



/* {{{ proto string get_data()
   */
PHP_METHOD(CairoImageSurface, get_data)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoImageSurface_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_data: not yet implemented"); RETURN_FALSE;

	RETURN_STRINGL("", 0, 1);
}
/* }}} get_data */



/* {{{ proto int get_format()
   */
PHP_METHOD(CairoImageSurface, get_format)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoImageSurface_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_format: not yet implemented"); RETURN_FALSE;

	RETURN_LONG(0);
}
/* }}} get_format */



/* {{{ proto int get_height()
   */
PHP_METHOD(CairoImageSurface, get_height)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoImageSurface_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_height: not yet implemented"); RETURN_FALSE;

	RETURN_LONG(0);
}
/* }}} get_height */



/* {{{ proto int get_stride()
   */
PHP_METHOD(CairoImageSurface, get_stride)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoImageSurface_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_stride: not yet implemented"); RETURN_FALSE;

	RETURN_LONG(0);
}
/* }}} get_stride */



/* {{{ proto int get_width()
   */
PHP_METHOD(CairoImageSurface, get_width)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoImageSurface_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_width: not yet implemented"); RETURN_FALSE;

	RETURN_LONG(0);
}
/* }}} get_width */


static zend_function_entry CairoImageSurface_methods[] = {
	PHP_ME(CairoImageSurface, __construct, CairoImageSurface____construct_args, /**/ZEND_ACC_PUBLIC | ZEND_ACC_CTOR)
	PHP_ME(CairoImageSurface, create_from_data, CairoImageSurface__create_from_data_args, /**/ZEND_ACC_PRIVATE)
	PHP_ME(CairoImageSurface, create_from_png, CairoImageSurface__create_from_png_args, /**/ZEND_ACC_PRIVATE)
	PHP_ME(CairoImageSurface, get_data, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoImageSurface, get_format, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoImageSurface, get_height, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoImageSurface, get_stride, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoImageSurface, get_width, NULL, /**/ZEND_ACC_PUBLIC)
	{ NULL, NULL, NULL }
};

/* }}} Methods */

static void class_init_CairoImageSurface(void)
{
	zend_class_entry ce;

	INIT_CLASS_ENTRY(ce, "CairoImageSurface", CairoImageSurface_methods);
	CairoImageSurface_ce_ptr = zend_register_internal_class_ex(&ce, NULL, "CairoSurface" TSRMLS_CC);
}

/* }}} Class CairoImageSurface */

/* {{{ Class CairoPDFSurface */

static zend_class_entry * CairoPDFSurface_ce_ptr = NULL;

/* {{{ Methods */


/* {{{ proto void construct(string file, float wpts, float hpts)
   */
PHP_METHOD(CairoPDFSurface, __construct)
{
	zend_class_entry * _this_ce;
	zval * _this_zval;

	const char * file = NULL;
	int file_len = 0;
	double wpts = 0.0;
	double hpts = 0.0;



	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "sdd", &file, &file_len, &wpts, &hpts) == FAILURE) {
		return;
	}

	_this_zval = getThis();
	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "__construct: not yet implemented"); RETURN_FALSE;

}
/* }}} __construct */



/* {{{ proto void set_size(float wptd, float hpts)
   */
PHP_METHOD(CairoPDFSurface, set_size)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double wptd = 0.0;
	double hpts = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Odd", &_this_zval, CairoPDFSurface_ce_ptr, &wptd, &hpts) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "set_size: not yet implemented"); RETURN_FALSE;

}
/* }}} set_size */


static zend_function_entry CairoPDFSurface_methods[] = {
	PHP_ME(CairoPDFSurface, __construct, CairoPDFSurface____construct_args, /**/ZEND_ACC_PUBLIC | ZEND_ACC_CTOR)
	PHP_ME(CairoPDFSurface, set_size, CairoPDFSurface__set_size_args, /**/ZEND_ACC_PUBLIC)
	{ NULL, NULL, NULL }
};

/* }}} Methods */

static void class_init_CairoPDFSurface(void)
{
	zend_class_entry ce;

	INIT_CLASS_ENTRY(ce, "CairoPDFSurface", CairoPDFSurface_methods);
	CairoPDFSurface_ce_ptr = zend_register_internal_class_ex(&ce, NULL, "CairoSurface" TSRMLS_CC);
}

/* }}} Class CairoPDFSurface */

/* {{{ Class CairoPSSurface */

static zend_class_entry * CairoPSSurface_ce_ptr = NULL;

/* {{{ Methods */


/* {{{ proto void construct(string file, float wpts, float hpts)
   */
PHP_METHOD(CairoPSSurface, __construct)
{
	zend_class_entry * _this_ce;
	zval * _this_zval;

	const char * file = NULL;
	int file_len = 0;
	double wpts = 0.0;
	double hpts = 0.0;



	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "sdd", &file, &file_len, &wpts, &hpts) == FAILURE) {
		return;
	}

	_this_zval = getThis();
	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "__construct: not yet implemented"); RETURN_FALSE;

}
/* }}} __construct */



/* {{{ proto void dsc_begin_page_setup()
   */
PHP_METHOD(CairoPSSurface, dsc_begin_page_setup)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoPSSurface_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "dsc_begin_page_setup: not yet implemented"); RETURN_FALSE;

}
/* }}} dsc_begin_page_setup */



/* {{{ proto void dsc_begin_setup()
   */
PHP_METHOD(CairoPSSurface, dsc_begin_setup)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoPSSurface_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "dsc_begin_setup: not yet implemented"); RETURN_FALSE;

}
/* }}} dsc_begin_setup */



/* {{{ proto void dsc_comment()
   */
PHP_METHOD(CairoPSSurface, dsc_comment)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoPSSurface_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "dsc_comment: not yet implemented"); RETURN_FALSE;

}
/* }}} dsc_comment */



/* {{{ proto array get_levels()
   */
PHP_METHOD(CairoPSSurface, get_levels)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoPSSurface_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	array_init(return_value);

	do {
		/* ONLY for CAIRO 1.6 
	} while (0);
}
/* }}} get_levels */



/* {{{ proto string get_level_string()
   */
PHP_METHOD(CairoPSSurface, get_level_string)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoPSSurface_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	do {
		/* ONLY for CAIRO 1.6*/
	} while (0);
}
/* }}} get_level_string */



/* {{{ proto void restrict_to_level(int level)
   */
PHP_METHOD(CairoPSSurface, restrict_to_level)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	long level = 0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Ol", &_this_zval, CairoPSSurface_ce_ptr, &level) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	do {
		/* ONLY for CAIRO 1.6 */
	} while (0);
}
/* }}} restrict_to_level */



/* {{{ proto void set_eps()
   */
PHP_METHOD(CairoPSSurface, set_eps)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoPSSurface_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	do {
		/* ONLY for CAIRO 1.6 */
	} while (0);
}
/* }}} set_eps */



/* {{{ proto void set_size(float wpts, float hpts)
   */
PHP_METHOD(CairoPSSurface, set_size)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;
	double wpts = 0.0;
	double hpts = 0.0;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Odd", &_this_zval, CairoPSSurface_ce_ptr, &wpts, &hpts) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "set_size: not yet implemented"); RETURN_FALSE;

}
/* }}} set_size */


static zend_function_entry CairoPSSurface_methods[] = {
	PHP_ME(CairoPSSurface, __construct, CairoPSSurface____construct_args, /**/ZEND_ACC_PUBLIC | ZEND_ACC_CTOR)
	PHP_ME(CairoPSSurface, dsc_begin_page_setup, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoPSSurface, dsc_begin_setup, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoPSSurface, dsc_comment, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoPSSurface, get_levels, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoPSSurface, get_level_string, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoPSSurface, restrict_to_level, CairoPSSurface__restrict_to_level_args, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoPSSurface, set_eps, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoPSSurface, set_size, CairoPSSurface__set_size_args, /**/ZEND_ACC_PUBLIC)
	{ NULL, NULL, NULL }
};

/* }}} Methods */

static void class_init_CairoPSSurface(void)
{
	zend_class_entry ce;

	INIT_CLASS_ENTRY(ce, "CairoPSSurface", CairoPSSurface_methods);
	CairoPSSurface_ce_ptr = zend_register_internal_class_ex(&ce, NULL, "CairoSurface" TSRMLS_CC);
}

/* }}} Class CairoPSSurface */

/* {{{ Class CairoQuartzSurface */

static zend_class_entry * CairoQuartzSurface_ce_ptr = NULL;

/* {{{ Methods */


/* {{{ proto void construct(float wpixels, float hpixels [, int format])
   */
PHP_METHOD(CairoQuartzSurface, __construct)
{
	zend_class_entry * _this_ce;
	zval * _this_zval;

	double wpixels = 0.0;
	double hpixels = 0.0;
	long format = 0;



	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "dd|l", &wpixels, &hpixels, &format) == FAILURE) {
		return;
	}

	_this_zval = getThis();
	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "__construct: not yet implemented"); RETURN_FALSE;

}
/* }}} __construct */


static zend_function_entry CairoQuartzSurface_methods[] = {
	PHP_ME(CairoQuartzSurface, __construct, CairoQuartzSurface____construct_args, /**/ZEND_ACC_PUBLIC | ZEND_ACC_CTOR)
	{ NULL, NULL, NULL }
};

/* }}} Methods */

static void class_init_CairoQuartzSurface(void)
{
	zend_class_entry ce;

	INIT_CLASS_ENTRY(ce, "CairoQuartzSurface", CairoQuartzSurface_methods);
	CairoQuartzSurface_ce_ptr = zend_register_internal_class_ex(&ce, NULL, "CairoSurface" TSRMLS_CC);
}

/* }}} Class CairoQuartzSurface */

/* {{{ Class CairoSVGSurface */

static zend_class_entry * CairoSVGSurface_ce_ptr = NULL;

/* {{{ Methods */


/* {{{ proto void construct(string file, float wpts, float hpts)
   */
PHP_METHOD(CairoSVGSurface, __construct)
{
	zend_class_entry * _this_ce;
	zval * _this_zval;

	const char * file = NULL;
	int file_len = 0;
	double wpts = 0.0;
	double hpts = 0.0;



	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "sdd", &file, &file_len, &wpts, &hpts) == FAILURE) {
		return;
	}

	_this_zval = getThis();
	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "__construct: not yet implemented"); RETURN_FALSE;

}
/* }}} __construct */


static zend_function_entry CairoSVGSurface_methods[] = {
	PHP_ME(CairoSVGSurface, __construct, CairoSVGSurface____construct_args, /**/ZEND_ACC_PUBLIC | ZEND_ACC_CTOR)
	{ NULL, NULL, NULL }
};

/* }}} Methods */

static void class_init_CairoSVGSurface(void)
{
	zend_class_entry ce;

	INIT_CLASS_ENTRY(ce, "CairoSVGSurface", CairoSVGSurface_methods);
	CairoSVGSurface_ce_ptr = zend_register_internal_class_ex(&ce, NULL, "CairoSurface" TSRMLS_CC);
}

/* }}} Class CairoSVGSurface */

/* {{{ Class CairoWin32Surface */

static zend_class_entry * CairoWin32Surface_ce_ptr = NULL;

/* {{{ Methods */


/* {{{ proto void construct(int hdc)
   */
PHP_METHOD(CairoWin32Surface, __construct)
{
	zend_class_entry * _this_ce;
	zval * _this_zval;

	long hdc = 0;



	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "l", &hdc) == FAILURE) {
		return;
	}

	_this_zval = getThis();
	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "__construct: not yet implemented"); RETURN_FALSE;

}
/* }}} __construct */


static zend_function_entry CairoWin32Surface_methods[] = {
	PHP_ME(CairoWin32Surface, __construct, NULL, /**/ZEND_ACC_PUBLIC | ZEND_ACC_CTOR)
	{ NULL, NULL, NULL }
};

/* }}} Methods */

static void class_init_CairoWin32Surface(void)
{
	zend_class_entry ce;

	INIT_CLASS_ENTRY(ce, "CairoWin32Surface", CairoWin32Surface_methods);
	CairoWin32Surface_ce_ptr = zend_register_internal_class_ex(&ce, NULL, "CairoSurface" TSRMLS_CC);
}

/* }}} Class CairoWin32Surface */

/* {{{ Class CairoXlibSurface */

static zend_class_entry * CairoXlibSurface_ce_ptr = NULL;

/* {{{ Methods */


/* {{{ proto void construct()
   */
PHP_METHOD(CairoXlibSurface, __construct)
{
	zend_class_entry * _this_ce;
	zval * _this_zval;



	if (ZEND_NUM_ARGS()>0)  {
		WRONG_PARAM_COUNT;
	}


	php_error(E_WARNING, "__construct: not yet implemented"); RETURN_FALSE;

}
/* }}} __construct */



/* {{{ proto int get_depth()
   */
PHP_METHOD(CairoXlibSurface, get_depth)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoXlibSurface_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_depth: not yet implemented"); RETURN_FALSE;

	RETURN_LONG(0);
}
/* }}} get_depth */



/* {{{ proto int get_height()
   */
PHP_METHOD(CairoXlibSurface, get_height)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoXlibSurface_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_height: not yet implemented"); RETURN_FALSE;

	RETURN_LONG(0);
}
/* }}} get_height */



/* {{{ proto int get_width()
   */
PHP_METHOD(CairoXlibSurface, get_width)
{
	zend_class_entry * _this_ce;

	zval * _this_zval = NULL;



	if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "O", &_this_zval, CairoXlibSurface_ce_ptr) == FAILURE) {
		return;
	}

	_this_ce = Z_OBJCE_P(_this_zval);


	php_error(E_WARNING, "get_width: not yet implemented"); RETURN_FALSE;

	RETURN_LONG(0);
}
/* }}} get_width */


static zend_function_entry CairoXlibSurface_methods[] = {
	PHP_ME(CairoXlibSurface, __construct, NULL, /**/ZEND_ACC_PUBLIC | ZEND_ACC_CTOR)
	PHP_ME(CairoXlibSurface, get_depth, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoXlibSurface, get_height, NULL, /**/ZEND_ACC_PUBLIC)
	PHP_ME(CairoXlibSurface, get_width, NULL, /**/ZEND_ACC_PUBLIC)
	{ NULL, NULL, NULL }
};

/* }}} Methods */

static void class_init_CairoXlibSurface(void)
{
	zend_class_entry ce;

	INIT_CLASS_ENTRY(ce, "CairoXlibSurface", CairoXlibSurface_methods);
	CairoXlibSurface_ce_ptr = zend_register_internal_class_ex(&ce, NULL, "CairoSurface" TSRMLS_CC);
}

/* }}} Class CairoXlibSurface */

/* {{{ Class CairoException */

static zend_class_entry * CairoException_ce_ptr = NULL;

/* {{{ Methods */

static zend_function_entry CairoException_methods[] = {
	{ NULL, NULL, NULL }
};

/* }}} Methods */

static void class_init_CairoException(void)
{
	zend_class_entry ce;

	INIT_CLASS_ENTRY(ce, "CairoException", CairoException_methods);
	CairoException_ce_ptr = zend_register_internal_class_ex(&ce, NULL, "Exception" TSRMLS_CC);
}

/* }}} Class CairoException */

/* }}} Class definitions*/

/* {{{ phpCairo_functions[] */
function_entry phpCairo_functions[] = {
	PHP_FE(cairo_version       , cairo_version_arg_info)
	PHP_FE(cairo_version_string, cairo_version_string_arg_info)
	{ NULL, NULL, NULL }
};
/* }}} */


/* {{{ phpCairo_module_entry
 */
zend_module_entry phpCairo_module_entry = {
	STANDARD_MODULE_HEADER,
	"phpCairo",
	phpCairo_functions,
	PHP_MINIT(phpCairo),     /* Replace with NULL if there is nothing to do at php startup   */ 
	PHP_MSHUTDOWN(phpCairo), /* Replace with NULL if there is nothing to do at php shutdown  */
	PHP_RINIT(phpCairo),     /* Replace with NULL if there is nothing to do at request start */
	PHP_RSHUTDOWN(phpCairo), /* Replace with NULL if there is nothing to do at request end   */
	PHP_MINFO(phpCairo),
	"0.0.1", 
	STANDARD_MODULE_PROPERTIES
};
/* }}} */

#ifdef COMPILE_DL_PHPCAIRO
ZEND_GET_MODULE(phpCairo)
#endif


/* {{{ PHP_MINIT_FUNCTION */
PHP_MINIT_FUNCTION(phpCairo)
{
	class_init_CairoContext();
	class_init_CairoFontFace();
	class_init_CairoFontOptions();
	class_init_CairoMatrix();
	class_init_CairoPattern();
	class_init_CairoGradient();
	class_init_CairoLinearGradient();
	class_init_CairoRadialGradient();
	class_init_CairoSolidPattern();
	class_init_CairoSurfacePattern();
	class_init_CairoScaledFont();
	class_init_CairoSurface();
	class_init_CairoImageSurface();
	class_init_CairoPDFSurface();
	class_init_CairoPSSurface();
	class_init_CairoQuartzSurface();
	class_init_CairoSVGSurface();
	class_init_CairoWin32Surface();
	class_init_CairoXlibSurface();
	class_init_CairoException();

	/* add your stuff here */

	return SUCCESS;
}
/* }}} */


/* {{{ PHP_MSHUTDOWN_FUNCTION */
PHP_MSHUTDOWN_FUNCTION(phpCairo)
{

	/* add your stuff here */

	return SUCCESS;
}
/* }}} */


/* {{{ PHP_RINIT_FUNCTION */
PHP_RINIT_FUNCTION(phpCairo)
{
	/* add your stuff here */

	return SUCCESS;
}
/* }}} */


/* {{{ PHP_RSHUTDOWN_FUNCTION */
PHP_RSHUTDOWN_FUNCTION(phpCairo)
{
	/* add your stuff here */

	return SUCCESS;
}
/* }}} */


/* {{{ PHP_MINFO_FUNCTION */
PHP_MINFO_FUNCTION(phpCairo)
{
	php_info_print_box_start(0);
	php_printf("<p>PHP bindings for Cairo Graphic Library</p>\n");
	php_printf("<p>Version 0.0.1devel (2008-05-06)</p>\n");
	php_printf("<p><b>Authors:</b></p>\n");
	php_printf("<p>Akshat Gupta &lt;g.akshat@gmail.com&gt; (lead)</p>\n");
	php_info_print_box_end();
	/* add your stuff here */

}
/* }}} */


/* {{{ proto int cairo_version()
   */
PHP_FUNCTION(cairo_version)
{



	if (ZEND_NUM_ARGS()>0)  {
		WRONG_PARAM_COUNT;
	}


	php_error(E_WARNING, "cairo_version: not yet implemented"); RETURN_FALSE;

	RETURN_LONG(0);
}
/* }}} cairo_version */


/* {{{ proto string cairo_version_string()
   */
PHP_FUNCTION(cairo_version_string)
{



	if (ZEND_NUM_ARGS()>0)  {
		WRONG_PARAM_COUNT;
	}


	php_error(E_WARNING, "cairo_version_string: not yet implemented"); RETURN_FALSE;

	RETURN_STRINGL("", 0, 1);
}
/* }}} cairo_version_string */

#endif /* HAVE_PHPCAIRO */


/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 * vim600: noet sw=4 ts=4 fdm=marker
 * vim<600: noet sw=4 ts=4
 */
