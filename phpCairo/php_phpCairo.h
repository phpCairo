/*
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.0 of the PHP license,       |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_0.txt.                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
   | Authors: Akshat Gupta <g.akshat@gmail.com>                           |
   +----------------------------------------------------------------------+
*/

/* $ Id: $ */ 

#ifndef PHP_PHPCAIRO_H
#define PHP_PHPCAIRO_H

#ifdef  __cplusplus
extern "C" {
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <php.h>

#ifdef HAVE_PHPCAIRO

#include <php_ini.h>
#include <SAPI.h>
#include <ext/standard/info.h>
#include <Zend/zend_extensions.h>
#ifdef  __cplusplus
} // extern "C" 
#endif
#ifdef  __cplusplus
extern "C" {
#endif

extern zend_module_entry phpCairo_module_entry;
#define phpext_phpCairo_ptr &phpCairo_module_entry

#ifdef PHP_WIN32
#define PHP_PHPCAIRO_API __declspec(dllexport)
#else
#define PHP_PHPCAIRO_API
#endif

PHP_MINIT_FUNCTION(phpCairo);
PHP_MSHUTDOWN_FUNCTION(phpCairo);
PHP_RINIT_FUNCTION(phpCairo);
PHP_RSHUTDOWN_FUNCTION(phpCairo);
PHP_MINFO_FUNCTION(phpCairo);

#ifdef ZTS
#include "TSRM.h"
#endif

#define FREE_RESOURCE(resource) zend_list_delete(Z_LVAL_P(resource))

#define PROP_GET_LONG(name)    Z_LVAL_P(zend_read_property(_this_ce, _this_zval, #name, strlen(#name), 1 TSRMLS_CC))
#define PROP_SET_LONG(name, l) zend_update_property_long(_this_ce, _this_zval, #name, strlen(#name), l TSRMLS_CC)

#define PROP_GET_DOUBLE(name)    Z_DVAL_P(zend_read_property(_this_ce, _this_zval, #name, strlen(#name), 1 TSRMLS_CC))
#define PROP_SET_DOUBLE(name, d) zend_update_property_double(_this_ce, _this_zval, #name, strlen(#name), d TSRMLS_CC)

#define PROP_GET_STRING(name)    Z_STRVAL_P(zend_read_property(_this_ce, _this_zval, #name, strlen(#name), 1 TSRMLS_CC))
#define PROP_GET_STRLEN(name)    Z_STRLEN_P(zend_read_property(_this_ce, _this_zval, #name, strlen(#name), 1 TSRMLS_CC))
#define PROP_SET_STRING(name, s) zend_update_property_string(_this_ce, _this_zval, #name, strlen(#name), s TSRMLS_CC)
#define PROP_SET_STRINGL(name, s, l) zend_update_property_stringl(_this_ce, _this_zval, #name, strlen(#name), s, l TSRMLS_CC)


PHP_FUNCTION(cairo_version);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(cairo_version_arg_info, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define cairo_version_arg_info NULL
#endif

PHP_FUNCTION(cairo_version_string);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(cairo_version_string_arg_info, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define cairo_version_string_arg_info NULL
#endif

PHP_METHOD(CairoContext, append_path);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__append_path_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_OBJ_INFO(0, p, , 1)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__append_path_args NULL
#endif

PHP_METHOD(CairoContext, arc);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__arc_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 5)
  ZEND_ARG_INFO(0, xc)
  ZEND_ARG_INFO(0, yc)
  ZEND_ARG_INFO(0, radius)
  ZEND_ARG_INFO(0, angle1)
  ZEND_ARG_INFO(0, angle2)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__arc_args NULL
#endif

PHP_METHOD(CairoContext, arc_negative);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__arc_negative_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 5)
  ZEND_ARG_INFO(0, xc)
  ZEND_ARG_INFO(0, yc)
  ZEND_ARG_INFO(0, radius)
  ZEND_ARG_INFO(0, angle1)
  ZEND_ARG_INFO(0, angle2)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__arc_negative_args NULL
#endif

PHP_METHOD(CairoContext, clip);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__clip_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__clip_args NULL
#endif

PHP_METHOD(CairoContext, clip_extents);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__clip_extents_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__clip_extents_args NULL
#endif

PHP_METHOD(CairoContext, clip_preserve);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__clip_preserve_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__clip_preserve_args NULL
#endif

PHP_METHOD(CairoContext, close_path);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__close_path_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__close_path_args NULL
#endif

PHP_METHOD(CairoContext, copy_clip_rectangle_list);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__copy_clip_rectangle_list_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__copy_clip_rectangle_list_args NULL
#endif

PHP_METHOD(CairoContext, copy_page);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__copy_page_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__copy_page_args NULL
#endif

PHP_METHOD(CairoContext, copy_path);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__copy_path_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__copy_path_args NULL
#endif

PHP_METHOD(CairoContext, copy_path_flat);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__copy_path_flat_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__copy_path_flat_args NULL
#endif

PHP_METHOD(CairoContext, curve_to);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__curve_to_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 6)
  ZEND_ARG_INFO(0, x1)
  ZEND_ARG_INFO(0, y1)
  ZEND_ARG_INFO(0, x2)
  ZEND_ARG_INFO(0, y2)
  ZEND_ARG_INFO(0, x3)
  ZEND_ARG_INFO(0, y3)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__curve_to_args NULL
#endif

PHP_METHOD(CairoContext, device_to_user);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__device_to_user_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, x)
  ZEND_ARG_INFO(0, y)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__device_to_user_args NULL
#endif

PHP_METHOD(CairoContext, device_to_user_distance);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__device_to_user_distance_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, x)
  ZEND_ARG_INFO(0, y)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__device_to_user_distance_args NULL
#endif

PHP_METHOD(CairoContext, fill);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__fill_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__fill_args NULL
#endif

PHP_METHOD(CairoContext, fill_extents);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__fill_extents_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__fill_extents_args NULL
#endif

PHP_METHOD(CairoContext, fill_preserve);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__fill_preserve_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__fill_preserve_args NULL
#endif

PHP_METHOD(CairoContext, font_extents);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__font_extents_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__font_extents_args NULL
#endif

PHP_METHOD(CairoContext, get_antialias);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_antialias_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__get_antialias_args NULL
#endif

PHP_METHOD(CairoContext, get_current_point);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_current_point_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__get_current_point_args NULL
#endif

PHP_METHOD(CairoContext, get_dash);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_dash_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__get_dash_args NULL
#endif

PHP_METHOD(CairoContext, get_dash_count);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_dash_count_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__get_dash_count_args NULL
#endif

PHP_METHOD(CairoContext, get_fill_rule);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_fill_rule_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__get_fill_rule_args NULL
#endif

PHP_METHOD(CairoContext, get_font_face);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_font_face_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__get_font_face_args NULL
#endif

PHP_METHOD(CairoContext, get_font_matrix);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_font_matrix_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__get_font_matrix_args NULL
#endif

PHP_METHOD(CairoContext, get_font_options);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_font_options_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__get_font_options_args NULL
#endif

PHP_METHOD(CairoContext, get_group_target);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_group_target_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__get_group_target_args NULL
#endif

PHP_METHOD(CairoContext, get_line_cap);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_line_cap_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__get_line_cap_args NULL
#endif

PHP_METHOD(CairoContext, get_line_join);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_line_join_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__get_line_join_args NULL
#endif

PHP_METHOD(CairoContext, get_line_width);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_line_width_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__get_line_width_args NULL
#endif

PHP_METHOD(CairoContext, get_matrix);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_matrix_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__get_matrix_args NULL
#endif

PHP_METHOD(CairoContext, get_matrix_limit);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_matrix_limit_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__get_matrix_limit_args NULL
#endif

PHP_METHOD(CairoContext, get_operator);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_operator_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__get_operator_args NULL
#endif

PHP_METHOD(CairoContext, get_scaled_font);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_scaled_font_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__get_scaled_font_args NULL
#endif

PHP_METHOD(CairoContext, get_source);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_source_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__get_source_args NULL
#endif

PHP_METHOD(CairoContext, get_target);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_target_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__get_target_args NULL
#endif

PHP_METHOD(CairoContext, get_tolerance);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__get_tolerance_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__get_tolerance_args NULL
#endif

PHP_METHOD(CairoContext, glyph_extents);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__glyph_extents_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_OBJ_INFO(0, obj, , 1)
  ZEND_ARG_INFO(0, num)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__glyph_extents_args NULL
#endif

PHP_METHOD(CairoContext, glyph_path);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__glyph_path_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_OBJ_INFO(0, obh, , 1)
  ZEND_ARG_INFO(0, num)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__glyph_path_args NULL
#endif

PHP_METHOD(CairoContext, has_current_point);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__has_current_point_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__has_current_point_args NULL
#endif

PHP_METHOD(CairoContext, identity_matrix);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__identity_matrix_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__identity_matrix_args NULL
#endif

PHP_METHOD(CairoContext, in_fill);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__in_fill_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, x)
  ZEND_ARG_INFO(0, y)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__in_fill_args NULL
#endif

PHP_METHOD(CairoContext, in_stroke);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__in_stroke_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, x)
  ZEND_ARG_INFO(0, y)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__in_stroke_args NULL
#endif

PHP_METHOD(CairoContext, line_to);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__line_to_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, x)
  ZEND_ARG_INFO(0, y)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__line_to_args NULL
#endif

PHP_METHOD(CairoContext, mask);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__mask_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_OBJ_INFO(0, p, , 1)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__mask_args NULL
#endif

PHP_METHOD(CairoContext, mask_surface);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__mask_surface_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_OBJ_INFO(0, s, , 1)
  ZEND_ARG_INFO(0, surface_x)
  ZEND_ARG_INFO(0, surface_y)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__mask_surface_args NULL
#endif

PHP_METHOD(CairoContext, move_to);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__move_to_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, x)
  ZEND_ARG_INFO(0, y)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__move_to_args NULL
#endif

PHP_METHOD(CairoContext, new_path);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__new_path_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__new_path_args NULL
#endif

PHP_METHOD(CairoContext, new_sub_path);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__new_sub_path_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__new_sub_path_args NULL
#endif

PHP_METHOD(CairoContext, paint);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__paint_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__paint_args NULL
#endif

PHP_METHOD(CairoContext, paint_with_alpha);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__paint_with_alpha_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, alpha)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__paint_with_alpha_args NULL
#endif

PHP_METHOD(CairoContext, path_extents);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__path_extents_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
  ZEND_ARG_OBJ_INFO(0, path, , 1)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__path_extents_args NULL
#endif

PHP_METHOD(CairoContext, pop_group);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__pop_group_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__pop_group_args NULL
#endif

PHP_METHOD(CairoContext, pop_group_to_source);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__pop_group_to_source_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__pop_group_to_source_args NULL
#endif

PHP_METHOD(CairoContext, push_group);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__push_group_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__push_group_args NULL
#endif

PHP_METHOD(CairoContext, push_group_with_content);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__push_group_with_content_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, content)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__push_group_with_content_args NULL
#endif

PHP_METHOD(CairoContext, rectangle);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__rectangle_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 4)
  ZEND_ARG_INFO(0, x)
  ZEND_ARG_INFO(0, y)
  ZEND_ARG_INFO(0, width)
  ZEND_ARG_INFO(0, height)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__rectangle_args NULL
#endif

PHP_METHOD(CairoContext, rel_curve_to);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__rel_curve_to_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 6)
  ZEND_ARG_INFO(0, x1)
  ZEND_ARG_INFO(0, y1)
  ZEND_ARG_INFO(0, x2)
  ZEND_ARG_INFO(0, y2)
  ZEND_ARG_INFO(0, x3)
  ZEND_ARG_INFO(0, y3)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__rel_curve_to_args NULL
#endif

PHP_METHOD(CairoContext, rel_line_to);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__rel_line_to_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, x)
  ZEND_ARG_INFO(0, y)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__rel_line_to_args NULL
#endif

PHP_METHOD(CairoContext, rel_move_to);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__rel_move_to_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, x)
  ZEND_ARG_INFO(0, y)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__rel_move_to_args NULL
#endif

PHP_METHOD(CairoContext, reset_clip);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__reset_clip_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__reset_clip_args NULL
#endif

PHP_METHOD(CairoContext, restore);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__restore_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__restore_args NULL
#endif

PHP_METHOD(CairoContext, rotate);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__rotate_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, angle)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__rotate_args NULL
#endif

PHP_METHOD(CairoContext, save);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__save_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__save_args NULL
#endif

PHP_METHOD(CairoContext, scale);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__scale_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, x)
  ZEND_ARG_INFO(0, y)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__scale_args NULL
#endif

PHP_METHOD(CairoContext, select_font_face);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__select_font_face_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_OBJ_INFO(0, obj, , 1)
  ZEND_ARG_INFO(0, slant)
  ZEND_ARG_INFO(0, weight)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__select_font_face_args NULL
#endif

PHP_METHOD(CairoContext, set_antialias);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_antialias_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
  ZEND_ARG_INFO(0, antialias)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__set_antialias_args NULL
#endif

PHP_METHOD(CairoContext, set_dash);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_dash_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_OBJ_INFO(0, dashes, , 1)
  ZEND_ARG_INFO(0, offset)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__set_dash_args NULL
#endif

PHP_METHOD(CairoContext, set_fill_rule);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_fill_rule_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, fill_rule)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__set_fill_rule_args NULL
#endif

PHP_METHOD(CairoContext, set_font_face);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_font_face_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
  ZEND_ARG_OBJ_INFO(0, obj, , 1)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__set_font_face_args NULL
#endif

PHP_METHOD(CairoContext, set_font_matrix);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_font_matrix_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_OBJ_INFO(0, matrix, , 1)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__set_font_matrix_args NULL
#endif

PHP_METHOD(CairoContext, set_font_options);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_font_options_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_OBJ_INFO(0, options, , 1)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__set_font_options_args NULL
#endif

PHP_METHOD(CairoContext, set_font_size);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_font_size_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, size)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__set_font_size_args NULL
#endif

PHP_METHOD(CairoContext, set_line_cap);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_line_cap_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, line_cap)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__set_line_cap_args NULL
#endif

PHP_METHOD(CairoContext, set_line_join);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_line_join_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, line_join)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__set_line_join_args NULL
#endif

PHP_METHOD(CairoContext, set_line_width);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_line_width_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, width)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__set_line_width_args NULL
#endif

PHP_METHOD(CairoContext, set_matrix);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_matrix_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_OBJ_INFO(0, matix, , 1)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__set_matrix_args NULL
#endif

PHP_METHOD(CairoContext, set_miter_limit);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_miter_limit_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, limit)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__set_miter_limit_args NULL
#endif

PHP_METHOD(CairoContext, set_operator);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_operator_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, op)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__set_operator_args NULL
#endif

PHP_METHOD(CairoContext, set_source);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_source_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_OBJ_INFO(0, p, , 1)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__set_source_args NULL
#endif

PHP_METHOD(CairoContext, set_source_rgb);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_source_rgb_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 3)
  ZEND_ARG_INFO(0, red)
  ZEND_ARG_INFO(0, green)
  ZEND_ARG_INFO(0, blue)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__set_source_rgb_args NULL
#endif

PHP_METHOD(CairoContext, set_source_rgba);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_source_rgba_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 3)
  ZEND_ARG_INFO(0, red)
  ZEND_ARG_INFO(0, green)
  ZEND_ARG_INFO(0, blue)
  ZEND_ARG_INFO(0, alpha)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__set_source_rgba_args NULL
#endif

PHP_METHOD(CairoContext, set_source_surface);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_source_surface_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_OBJ_INFO(0, surface, , 1)
  ZEND_ARG_INFO(0, x)
  ZEND_ARG_INFO(0, y)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__set_source_surface_args NULL
#endif

PHP_METHOD(CairoContext, set_tolerance);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__set_tolerance_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, tolerance)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__set_tolerance_args NULL
#endif

PHP_METHOD(CairoContext, show_glyphs);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__show_glyphs_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_OBJ_INFO(0, glyphs, , 1)
  ZEND_ARG_INFO(0, num_glyphs)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__show_glyphs_args NULL
#endif

PHP_METHOD(CairoContext, show_page);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__show_page_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__show_page_args NULL
#endif

PHP_METHOD(CairoContext, show_text);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__show_text_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_OBJ_INFO(0, obj, , 1)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__show_text_args NULL
#endif

PHP_METHOD(CairoContext, stroke);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__stroke_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__stroke_args NULL
#endif

PHP_METHOD(CairoContext, stroke_extents);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__stroke_extents_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__stroke_extents_args NULL
#endif

PHP_METHOD(CairoContext, stroke_preserve);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__stroke_preserve_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__stroke_preserve_args NULL
#endif

PHP_METHOD(CairoContext, text_extents);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__text_extents_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_OBJ_INFO(0, extents, , 1)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__text_extents_args NULL
#endif

PHP_METHOD(CairoContext, text_path);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__text_path_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_OBJ_INFO(0, obj, , 1)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__text_path_args NULL
#endif

PHP_METHOD(CairoContext, transform);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__transform_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_OBJ_INFO(0, matrix, , 1)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__transform_args NULL
#endif

PHP_METHOD(CairoContext, translate);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__translate_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, tx)
  ZEND_ARG_INFO(0, ty)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__translate_args NULL
#endif

PHP_METHOD(CairoContext, user_to_device);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__user_to_device_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, x)
  ZEND_ARG_INFO(0, y)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__user_to_device_args NULL
#endif

PHP_METHOD(CairoContext, user_to_device_distance);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoContext__user_to_device_distance_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, dx)
  ZEND_ARG_INFO(0, dy)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoContext__user_to_device_distance_args NULL
#endif

PHP_METHOD(CairoFontFace, __construct);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoFontFace____construct_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoFontFace____construct_args NULL
#endif

PHP_METHOD(CairoFontFace, __destruct);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoFontFace____destruct_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoFontFace____destruct_args NULL
#endif

PHP_METHOD(CairoFontOptions, __construct);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoFontOptions____construct_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoFontOptions____construct_args NULL
#endif

PHP_METHOD(CairoFontOptions, get_antialias);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoFontOptions__get_antialias_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoFontOptions__get_antialias_args NULL
#endif

PHP_METHOD(CairoFontOptions, get_hint_metrics);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoFontOptions__get_hint_metrics_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoFontOptions__get_hint_metrics_args NULL
#endif

PHP_METHOD(CairoFontOptions, get_hint_style);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoFontOptions__get_hint_style_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoFontOptions__get_hint_style_args NULL
#endif

PHP_METHOD(CairoFontOptions, get_subpixel_order);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoFontOptions__get_subpixel_order_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoFontOptions__get_subpixel_order_args NULL
#endif

PHP_METHOD(CairoFontOptions, set_antialias);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoFontOptions__set_antialias_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
  ZEND_ARG_INFO(0, aa)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoFontOptions__set_antialias_args NULL
#endif

PHP_METHOD(CairoFontOptions, set_hint_metrics);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoFontOptions__set_hint_metrics_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
  ZEND_ARG_INFO(0, hm)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoFontOptions__set_hint_metrics_args NULL
#endif

PHP_METHOD(CairoFontOptions, set_hint_style);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoFontOptions__set_hint_style_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
  ZEND_ARG_INFO(0, hs)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoFontOptions__set_hint_style_args NULL
#endif

PHP_METHOD(CairoFontOptions, set_subpixel_order);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoFontOptions__set_subpixel_order_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
  ZEND_ARG_INFO(0, so)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoFontOptions__set_subpixel_order_args NULL
#endif

PHP_METHOD(CairoMatrix, __construct);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoMatrix____construct_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 5)
  ZEND_ARG_INFO(0, yx)
  ZEND_ARG_INFO(0, xy)
  ZEND_ARG_INFO(0, yy)
  ZEND_ARG_INFO(0, x0)
  ZEND_ARG_INFO(0, y0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoMatrix____construct_args NULL
#endif

PHP_METHOD(CairoMatrix, init_rotate);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoMatrix__init_rotate_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, radians)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoMatrix__init_rotate_args NULL
#endif

PHP_METHOD(CairoMatrix, invert);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoMatrix__invert_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoMatrix__invert_args NULL
#endif

PHP_METHOD(CairoMatrix, multiply);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoMatrix__multiply_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_OBJ_INFO(0, o2, , 1)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoMatrix__multiply_args NULL
#endif

PHP_METHOD(CairoMatrix, rotate);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoMatrix__rotate_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, radians)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoMatrix__rotate_args NULL
#endif

PHP_METHOD(CairoMatrix, scale);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoMatrix__scale_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, sx)
  ZEND_ARG_INFO(0, xy)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoMatrix__scale_args NULL
#endif

PHP_METHOD(CairoMatrix, transform_distance);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoMatrix__transform_distance_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, dx)
  ZEND_ARG_INFO(0, dy)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoMatrix__transform_distance_args NULL
#endif

PHP_METHOD(CairoMatrix, transform_point);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoMatrix__transform_point_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, x)
  ZEND_ARG_INFO(0, y)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoMatrix__transform_point_args NULL
#endif

PHP_METHOD(CairoMatrix, translate);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoMatrix__translate_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, tx)
  ZEND_ARG_INFO(0, ty)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoMatrix__translate_args NULL
#endif

PHP_METHOD(CairoPattern, __construct);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoPattern____construct_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoPattern____construct_args NULL
#endif

PHP_METHOD(CairoPattern, get_matrix);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoPattern__get_matrix_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoPattern__get_matrix_args NULL
#endif

PHP_METHOD(CairoPattern, set_matrix);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoPattern__set_matrix_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_OBJ_INFO(0, m, , 1)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoPattern__set_matrix_args NULL
#endif

PHP_METHOD(CairoGradient, __contruct);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoGradient____contruct_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoGradient____contruct_args NULL
#endif

PHP_METHOD(CairoGradient, add_color_stop_rgb);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoGradient__add_color_stop_rgb_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 4)
  ZEND_ARG_INFO(0, offset)
  ZEND_ARG_INFO(0, red)
  ZEND_ARG_INFO(0, green)
  ZEND_ARG_INFO(0, blue)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoGradient__add_color_stop_rgb_args NULL
#endif

PHP_METHOD(CairoGradient, add_color_stop_rgba);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoGradient__add_color_stop_rgba_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 5)
  ZEND_ARG_INFO(0, offset)
  ZEND_ARG_INFO(0, red)
  ZEND_ARG_INFO(0, green)
  ZEND_ARG_INFO(0, blue)
  ZEND_ARG_INFO(0, alpha)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoGradient__add_color_stop_rgba_args NULL
#endif

PHP_METHOD(CairoLinearGradient, __construct);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoLinearGradient____construct_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 3)
  ZEND_ARG_INFO(0, y0)
  ZEND_ARG_INFO(0, x1)
  ZEND_ARG_INFO(0, y1)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoLinearGradient____construct_args NULL
#endif

PHP_METHOD(CairoLinearGradient, get_linear_points);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoLinearGradient__get_linear_points_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoLinearGradient__get_linear_points_args NULL
#endif

PHP_METHOD(CairoRadialGradient, __constuct);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoRadialGradient____constuct_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 6)
  ZEND_ARG_INFO(0, cx0)
  ZEND_ARG_INFO(0, cy0)
  ZEND_ARG_INFO(0, radius0)
  ZEND_ARG_INFO(0, cx1)
  ZEND_ARG_INFO(0, cy1)
  ZEND_ARG_INFO(0, radius1)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoRadialGradient____constuct_args NULL
#endif

PHP_METHOD(CairoRadialGradient, get_radial_circles);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoRadialGradient__get_radial_circles_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoRadialGradient__get_radial_circles_args NULL
#endif

PHP_METHOD(CairoSolidPattern, __construct);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoSolidPattern____construct_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, g)
  ZEND_ARG_INFO(0, b)
  ZEND_ARG_INFO(0, a)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoSolidPattern____construct_args NULL
#endif

PHP_METHOD(CairoSolidPattern, get_rgba);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoSolidPattern__get_rgba_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoSolidPattern__get_rgba_args NULL
#endif

PHP_METHOD(CairoSurfacePattern, __construct);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoSurfacePattern____construct_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoSurfacePattern____construct_args NULL
#endif

PHP_METHOD(CairoSurfacePattern, get_extend);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoSurfacePattern__get_extend_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoSurfacePattern__get_extend_args NULL
#endif

PHP_METHOD(CairoSurfacePattern, get_filter);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoSurfacePattern__get_filter_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoSurfacePattern__get_filter_args NULL
#endif

PHP_METHOD(CairoSurfacePattern, get_surface);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoSurfacePattern__get_surface_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoSurfacePattern__get_surface_args NULL
#endif

PHP_METHOD(CairoSurfacePattern, set_extend);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoSurfacePattern__set_extend_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, extend)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoSurfacePattern__set_extend_args NULL
#endif

PHP_METHOD(CairoSurfacePattern, set_filter);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoSurfacePattern__set_filter_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, filter)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoSurfacePattern__set_filter_args NULL
#endif

PHP_METHOD(CairoScaledFont, __construct);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoScaledFont____construct_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 3)
  ZEND_ARG_OBJ_INFO(0, mx1, , 1)
  ZEND_ARG_OBJ_INFO(0, mx2, , 1)
  ZEND_ARG_OBJ_INFO(0, fo, , 1)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoScaledFont____construct_args NULL
#endif

PHP_METHOD(CairoScaledFont, extents);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoScaledFont__extents_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoScaledFont__extents_args NULL
#endif

PHP_METHOD(CairoScaledFont, get_font_face);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoScaledFont__get_font_face_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoScaledFont__get_font_face_args NULL
#endif

PHP_METHOD(CairoScaledFont, text_extents);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoScaledFont__text_extents_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, str)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoScaledFont__text_extents_args NULL
#endif

PHP_METHOD(CairoSurface, __construct);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoSurface____construct_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoSurface____construct_args NULL
#endif

PHP_METHOD(CairoSurface, create_similar);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoSurface__create_similar_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 3)
  ZEND_ARG_INFO(0, content)
  ZEND_ARG_INFO(0, width)
  ZEND_ARG_INFO(0, height)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoSurface__create_similar_args NULL
#endif

PHP_METHOD(CairoSurface, finish);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoSurface__finish_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoSurface__finish_args NULL
#endif

PHP_METHOD(CairoSurface, flush);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoSurface__flush_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoSurface__flush_args NULL
#endif

PHP_METHOD(CairoSurface, get_content);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoSurface__get_content_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoSurface__get_content_args NULL
#endif

PHP_METHOD(CairoSurface, get_device_offset);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoSurface__get_device_offset_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoSurface__get_device_offset_args NULL
#endif

PHP_METHOD(CairoSurface, get_font_options);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoSurface__get_font_options_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoSurface__get_font_options_args NULL
#endif

PHP_METHOD(CairoSurface, mark_dirty_rectangle);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoSurface__mark_dirty_rectangle_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
  ZEND_ARG_INFO(0, x)
  ZEND_ARG_INFO(0, y)
  ZEND_ARG_INFO(0, width)
  ZEND_ARG_INFO(0, height)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoSurface__mark_dirty_rectangle_args NULL
#endif

PHP_METHOD(CairoSurface, set_device_offset);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoSurface__set_device_offset_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, x_offset)
  ZEND_ARG_INFO(0, y_offset)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoSurface__set_device_offset_args NULL
#endif

PHP_METHOD(CairoSurface, set_fallback_resolution);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoSurface__set_fallback_resolution_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, x_ppi)
  ZEND_ARG_INFO(0, y_ppi)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoSurface__set_fallback_resolution_args NULL
#endif

PHP_METHOD(CairoSurface, write_to_png);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoSurface__write_to_png_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, file)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoSurface__write_to_png_args NULL
#endif

PHP_METHOD(CairoImageSurface, __construct);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoImageSurface____construct_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, widthm)
  ZEND_ARG_INFO(0, height)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoImageSurface____construct_args NULL
#endif

PHP_METHOD(CairoImageSurface, create_from_data);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoImageSurface__create_from_data_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 4)
  ZEND_ARG_OBJ_INFO(0, obj, , 1)
  ZEND_ARG_INFO(0, format)
  ZEND_ARG_INFO(0, width)
  ZEND_ARG_INFO(0, height)
  ZEND_ARG_INFO(0, stride)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoImageSurface__create_from_data_args NULL
#endif

PHP_METHOD(CairoImageSurface, create_from_png);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoImageSurface__create_from_png_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, file)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoImageSurface__create_from_png_args NULL
#endif

PHP_METHOD(CairoImageSurface, get_data);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoImageSurface__get_data_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoImageSurface__get_data_args NULL
#endif

PHP_METHOD(CairoImageSurface, get_format);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoImageSurface__get_format_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoImageSurface__get_format_args NULL
#endif

PHP_METHOD(CairoImageSurface, get_height);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoImageSurface__get_height_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoImageSurface__get_height_args NULL
#endif

PHP_METHOD(CairoImageSurface, get_stride);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoImageSurface__get_stride_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoImageSurface__get_stride_args NULL
#endif

PHP_METHOD(CairoImageSurface, get_width);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoImageSurface__get_width_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoImageSurface__get_width_args NULL
#endif

PHP_METHOD(CairoPDFSurface, __construct);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoPDFSurface____construct_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, wpts)
  ZEND_ARG_INFO(0, hpts)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoPDFSurface____construct_args NULL
#endif

PHP_METHOD(CairoPDFSurface, set_size);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoPDFSurface__set_size_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, wptd)
  ZEND_ARG_INFO(0, hpts)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoPDFSurface__set_size_args NULL
#endif

PHP_METHOD(CairoPSSurface, __construct);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoPSSurface____construct_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, wpts)
  ZEND_ARG_INFO(0, hpts)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoPSSurface____construct_args NULL
#endif

PHP_METHOD(CairoPSSurface, dsc_begin_page_setup);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoPSSurface__dsc_begin_page_setup_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoPSSurface__dsc_begin_page_setup_args NULL
#endif

PHP_METHOD(CairoPSSurface, dsc_begin_setup);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoPSSurface__dsc_begin_setup_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoPSSurface__dsc_begin_setup_args NULL
#endif

PHP_METHOD(CairoPSSurface, dsc_comment);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoPSSurface__dsc_comment_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoPSSurface__dsc_comment_args NULL
#endif

PHP_METHOD(CairoPSSurface, get_levels);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoPSSurface__get_levels_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoPSSurface__get_levels_args NULL
#endif

PHP_METHOD(CairoPSSurface, get_level_string);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoPSSurface__get_level_string_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoPSSurface__get_level_string_args NULL
#endif

PHP_METHOD(CairoPSSurface, restrict_to_level);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoPSSurface__restrict_to_level_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, level)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoPSSurface__restrict_to_level_args NULL
#endif

PHP_METHOD(CairoPSSurface, set_eps);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoPSSurface__set_eps_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoPSSurface__set_eps_args NULL
#endif

PHP_METHOD(CairoPSSurface, set_size);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoPSSurface__set_size_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, wpts)
  ZEND_ARG_INFO(0, hpts)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoPSSurface__set_size_args NULL
#endif

PHP_METHOD(CairoQuartzSurface, __construct);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoQuartzSurface____construct_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, hpixels)
  ZEND_ARG_INFO(0, format)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoQuartzSurface____construct_args NULL
#endif

PHP_METHOD(CairoSVGSurface, __construct);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoSVGSurface____construct_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, wpts)
  ZEND_ARG_INFO(0, hpts)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoSVGSurface____construct_args NULL
#endif

PHP_METHOD(CairoWin32Surface, __construct);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoWin32Surface____construct_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoWin32Surface____construct_args NULL
#endif

PHP_METHOD(CairoXlibSurface, __construct);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoXlibSurface____construct_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoXlibSurface____construct_args NULL
#endif

PHP_METHOD(CairoXlibSurface, get_depth);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoXlibSurface__get_depth_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoXlibSurface__get_depth_args NULL
#endif

PHP_METHOD(CairoXlibSurface, get_height);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoXlibSurface__get_height_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoXlibSurface__get_height_args NULL
#endif

PHP_METHOD(CairoXlibSurface, get_width);
#if (PHP_MAJOR_VERSION >= 5)
ZEND_BEGIN_ARG_INFO_EX(CairoXlibSurface__get_width_args, ZEND_SEND_BY_VAL, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()
#else /* PHP 4.x */
#define CairoXlibSurface__get_width_args NULL
#endif

#ifdef  __cplusplus
} // extern "C" 
#endif

#endif /* PHP_HAVE_PHPCAIRO */

#endif /* PHP_PHPCAIRO_H */


/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 * vim600: noet sw=4 ts=4 fdm=marker
 * vim<600: noet sw=4 ts=4
 */
